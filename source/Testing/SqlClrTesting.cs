﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestProject1
{
    [TestClass()]
    public class SqlClrTesting : SqlDatabaseTestClass
    {

        public SqlClrTesting()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        [TestMethod()]
        public void restore_KillProcessesClr()
        {
            SqlDatabaseTestActions testActions = this.restore_KillProcessesClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void restore_RestrictAccessClr()
        {
            SqlDatabaseTestActions testActions = this.restore_RestrictAccessClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void restore_RunScriptsClr()
        {
            SqlDatabaseTestActions testActions = this.restore_RunScriptsClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void restore_RestoreDatabaseClr()
        {
            SqlDatabaseTestActions testActions = this.restore_RestoreDatabaseClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void permissions_ConfigureDatabaseSchemaUserPermissionsClr()
        {
            SqlDatabaseTestActions testActions = this.permissions_ConfigureDatabaseSchemaUserPermissionsClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void permissions_ConfigureDatabaseUserClr()
        {
            SqlDatabaseTestActions testActions = this.permissions_ConfigureDatabaseUserClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void permissions_ConfigureDatabaseUserRoleMembershipClr()
        {
            SqlDatabaseTestActions testActions = this.permissions_ConfigureDatabaseUserRoleMembershipClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void permissions_ConfigureInstanceLoginClr()
        {
            SqlDatabaseTestActions testActions = this.permissions_ConfigureInstanceLoginClrData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }









        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction restore_KillProcessesClr_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SqlClrTesting));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition1;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction restore_RestrictAccessClr_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition2;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction restore_RunScriptsClr_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition3;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction restore_RestoreDatabaseClr_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition8;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction permissions_ConfigureDatabaseSchemaUserPermissionsClr_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition4;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction permissions_ConfigureDatabaseUserClr_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition5;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction permissions_ConfigureDatabaseUserRoleMembershipClr_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition6;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction permissions_ConfigureInstanceLoginClr_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition7;
            this.restore_KillProcessesClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.restore_RestrictAccessClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.restore_RunScriptsClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.restore_RestoreDatabaseClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.permissions_ConfigureDatabaseSchemaUserPermissionsClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.permissions_ConfigureDatabaseUserClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.permissions_ConfigureDatabaseUserRoleMembershipClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.permissions_ConfigureInstanceLoginClrData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            restore_KillProcessesClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition1 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            restore_RestrictAccessClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition2 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            restore_RunScriptsClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition3 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            restore_RestoreDatabaseClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition8 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            permissions_ConfigureDatabaseSchemaUserPermissionsClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition4 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            permissions_ConfigureDatabaseUserClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition5 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            permissions_ConfigureDatabaseUserRoleMembershipClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition6 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            permissions_ConfigureInstanceLoginClr_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition7 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // restore_KillProcessesClr_TestAction
            // 
            restore_KillProcessesClr_TestAction.Conditions.Add(scalarValueCondition1);
            resources.ApplyResources(restore_KillProcessesClr_TestAction, "restore_KillProcessesClr_TestAction");
            // 
            // scalarValueCondition1
            // 
            scalarValueCondition1.ColumnNumber = 1;
            scalarValueCondition1.Enabled = true;
            scalarValueCondition1.ExpectedValue = "1";
            scalarValueCondition1.Name = "scalarValueCondition1";
            scalarValueCondition1.NullExpected = false;
            scalarValueCondition1.ResultSet = 1;
            scalarValueCondition1.RowNumber = 1;
            // 
            // restore_RestrictAccessClr_TestAction
            // 
            restore_RestrictAccessClr_TestAction.Conditions.Add(scalarValueCondition2);
            resources.ApplyResources(restore_RestrictAccessClr_TestAction, "restore_RestrictAccessClr_TestAction");
            // 
            // scalarValueCondition2
            // 
            scalarValueCondition2.ColumnNumber = 1;
            scalarValueCondition2.Enabled = true;
            scalarValueCondition2.ExpectedValue = "1";
            scalarValueCondition2.Name = "scalarValueCondition2";
            scalarValueCondition2.NullExpected = false;
            scalarValueCondition2.ResultSet = 1;
            scalarValueCondition2.RowNumber = 1;
            // 
            // restore_RunScriptsClr_TestAction
            // 
            restore_RunScriptsClr_TestAction.Conditions.Add(scalarValueCondition3);
            resources.ApplyResources(restore_RunScriptsClr_TestAction, "restore_RunScriptsClr_TestAction");
            // 
            // scalarValueCondition3
            // 
            scalarValueCondition3.ColumnNumber = 1;
            scalarValueCondition3.Enabled = true;
            scalarValueCondition3.ExpectedValue = "1";
            scalarValueCondition3.Name = "scalarValueCondition3";
            scalarValueCondition3.NullExpected = false;
            scalarValueCondition3.ResultSet = 1;
            scalarValueCondition3.RowNumber = 1;
            // 
            // restore_RestoreDatabaseClr_TestAction
            // 
            restore_RestoreDatabaseClr_TestAction.Conditions.Add(scalarValueCondition8);
            resources.ApplyResources(restore_RestoreDatabaseClr_TestAction, "restore_RestoreDatabaseClr_TestAction");
            // 
            // scalarValueCondition8
            // 
            scalarValueCondition8.ColumnNumber = 1;
            scalarValueCondition8.Enabled = true;
            scalarValueCondition8.ExpectedValue = "1";
            scalarValueCondition8.Name = "scalarValueCondition8";
            scalarValueCondition8.NullExpected = false;
            scalarValueCondition8.ResultSet = 1;
            scalarValueCondition8.RowNumber = 1;
            // 
            // permissions_ConfigureDatabaseSchemaUserPermissionsClr_TestAction
            // 
            permissions_ConfigureDatabaseSchemaUserPermissionsClr_TestAction.Conditions.Add(scalarValueCondition4);
            resources.ApplyResources(permissions_ConfigureDatabaseSchemaUserPermissionsClr_TestAction, "permissions_ConfigureDatabaseSchemaUserPermissionsClr_TestAction");
            // 
            // scalarValueCondition4
            // 
            scalarValueCondition4.ColumnNumber = 1;
            scalarValueCondition4.Enabled = true;
            scalarValueCondition4.ExpectedValue = "1";
            scalarValueCondition4.Name = "scalarValueCondition4";
            scalarValueCondition4.NullExpected = false;
            scalarValueCondition4.ResultSet = 1;
            scalarValueCondition4.RowNumber = 1;
            // 
            // permissions_ConfigureDatabaseUserClr_TestAction
            // 
            permissions_ConfigureDatabaseUserClr_TestAction.Conditions.Add(scalarValueCondition5);
            resources.ApplyResources(permissions_ConfigureDatabaseUserClr_TestAction, "permissions_ConfigureDatabaseUserClr_TestAction");
            // 
            // scalarValueCondition5
            // 
            scalarValueCondition5.ColumnNumber = 1;
            scalarValueCondition5.Enabled = true;
            scalarValueCondition5.ExpectedValue = "1";
            scalarValueCondition5.Name = "scalarValueCondition5";
            scalarValueCondition5.NullExpected = false;
            scalarValueCondition5.ResultSet = 1;
            scalarValueCondition5.RowNumber = 1;
            // 
            // permissions_ConfigureDatabaseUserRoleMembershipClr_TestAction
            // 
            permissions_ConfigureDatabaseUserRoleMembershipClr_TestAction.Conditions.Add(scalarValueCondition6);
            resources.ApplyResources(permissions_ConfigureDatabaseUserRoleMembershipClr_TestAction, "permissions_ConfigureDatabaseUserRoleMembershipClr_TestAction");
            // 
            // scalarValueCondition6
            // 
            scalarValueCondition6.ColumnNumber = 1;
            scalarValueCondition6.Enabled = true;
            scalarValueCondition6.ExpectedValue = "1";
            scalarValueCondition6.Name = "scalarValueCondition6";
            scalarValueCondition6.NullExpected = false;
            scalarValueCondition6.ResultSet = 1;
            scalarValueCondition6.RowNumber = 1;
            // 
            // permissions_ConfigureInstanceLoginClr_TestAction
            // 
            permissions_ConfigureInstanceLoginClr_TestAction.Conditions.Add(scalarValueCondition7);
            resources.ApplyResources(permissions_ConfigureInstanceLoginClr_TestAction, "permissions_ConfigureInstanceLoginClr_TestAction");
            // 
            // scalarValueCondition7
            // 
            scalarValueCondition7.ColumnNumber = 1;
            scalarValueCondition7.Enabled = true;
            scalarValueCondition7.ExpectedValue = "1";
            scalarValueCondition7.Name = "scalarValueCondition7";
            scalarValueCondition7.NullExpected = false;
            scalarValueCondition7.ResultSet = 1;
            scalarValueCondition7.RowNumber = 1;
            // 
            // restore_KillProcessesClrData
            // 
            this.restore_KillProcessesClrData.PosttestAction = null;
            this.restore_KillProcessesClrData.PretestAction = null;
            this.restore_KillProcessesClrData.TestAction = restore_KillProcessesClr_TestAction;
            // 
            // restore_RestrictAccessClrData
            // 
            this.restore_RestrictAccessClrData.PosttestAction = null;
            this.restore_RestrictAccessClrData.PretestAction = null;
            this.restore_RestrictAccessClrData.TestAction = restore_RestrictAccessClr_TestAction;
            // 
            // restore_RunScriptsClrData
            // 
            this.restore_RunScriptsClrData.PosttestAction = null;
            this.restore_RunScriptsClrData.PretestAction = null;
            this.restore_RunScriptsClrData.TestAction = restore_RunScriptsClr_TestAction;
            // 
            // restore_RestoreDatabaseClrData
            // 
            this.restore_RestoreDatabaseClrData.PosttestAction = null;
            this.restore_RestoreDatabaseClrData.PretestAction = null;
            this.restore_RestoreDatabaseClrData.TestAction = restore_RestoreDatabaseClr_TestAction;
            // 
            // permissions_ConfigureDatabaseSchemaUserPermissionsClrData
            // 
            this.permissions_ConfigureDatabaseSchemaUserPermissionsClrData.PosttestAction = null;
            this.permissions_ConfigureDatabaseSchemaUserPermissionsClrData.PretestAction = null;
            this.permissions_ConfigureDatabaseSchemaUserPermissionsClrData.TestAction = permissions_ConfigureDatabaseSchemaUserPermissionsClr_TestAction;
            // 
            // permissions_ConfigureDatabaseUserClrData
            // 
            this.permissions_ConfigureDatabaseUserClrData.PosttestAction = null;
            this.permissions_ConfigureDatabaseUserClrData.PretestAction = null;
            this.permissions_ConfigureDatabaseUserClrData.TestAction = permissions_ConfigureDatabaseUserClr_TestAction;
            // 
            // permissions_ConfigureDatabaseUserRoleMembershipClrData
            // 
            this.permissions_ConfigureDatabaseUserRoleMembershipClrData.PosttestAction = null;
            this.permissions_ConfigureDatabaseUserRoleMembershipClrData.PretestAction = null;
            this.permissions_ConfigureDatabaseUserRoleMembershipClrData.TestAction = permissions_ConfigureDatabaseUserRoleMembershipClr_TestAction;
            // 
            // permissions_ConfigureInstanceLoginClrData
            // 
            this.permissions_ConfigureInstanceLoginClrData.PosttestAction = null;
            this.permissions_ConfigureInstanceLoginClrData.PretestAction = null;
            this.permissions_ConfigureInstanceLoginClrData.TestAction = permissions_ConfigureInstanceLoginClr_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion

        private SqlDatabaseTestActions restore_KillProcessesClrData;
        private SqlDatabaseTestActions restore_RestrictAccessClrData;
        private SqlDatabaseTestActions restore_RunScriptsClrData;
        private SqlDatabaseTestActions restore_RestoreDatabaseClrData;
        private SqlDatabaseTestActions permissions_ConfigureDatabaseSchemaUserPermissionsClrData;
        private SqlDatabaseTestActions permissions_ConfigureDatabaseUserClrData;
        private SqlDatabaseTestActions permissions_ConfigureDatabaseUserRoleMembershipClrData;
        private SqlDatabaseTestActions permissions_ConfigureInstanceLoginClrData;
    }
}
