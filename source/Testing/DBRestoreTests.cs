﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting;
using Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestProject1
{
    [TestClass()]
    public class DBRestoreTests : SqlDatabaseTestClass
    {

        public DBRestoreTests()
        {
            InitializeComponent();
        }

        [TestInitialize()]
        public void TestInitialize()
        {
            base.InitializeTest();
        }
        [TestCleanup()]
        public void TestCleanup()
        {
            base.CleanupTest();
        }

        #region Designer support code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction infrastructure_GetConfigValueTest_TestAction;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DBRestoreTests));
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition1;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction logging_AddEventLogTest_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ChecksumCondition checksumCondition2;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction logging_ArchiveEventLogTest_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ChecksumCondition checksumCondition3;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction infrastructure_CheckDBInstanceExistsTest_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition2;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction permissions_CheckForDBSchemaUserPermissionsTest_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition4;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction permissions_CheckForDBUserRoleMembershipTest_TestAction;
            Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition scalarValueCondition5;
            this.infrastructure_GetConfigValueTestData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.logging_AddEventLogTestData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.logging_ArchiveEventLogTestData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.infrastructure_CheckDBInstanceExistsTestData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.permissions_CheckForDBSchemaUserPermissionsTestData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            this.permissions_CheckForDBUserRoleMembershipTestData = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestActions();
            infrastructure_GetConfigValueTest_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition1 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            logging_AddEventLogTest_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            checksumCondition2 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ChecksumCondition();
            logging_ArchiveEventLogTest_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            checksumCondition3 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ChecksumCondition();
            infrastructure_CheckDBInstanceExistsTest_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition2 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            permissions_CheckForDBSchemaUserPermissionsTest_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition4 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            permissions_CheckForDBUserRoleMembershipTest_TestAction = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.SqlDatabaseTestAction();
            scalarValueCondition5 = new Microsoft.Data.Tools.Schema.Sql.UnitTesting.Conditions.ScalarValueCondition();
            // 
            // infrastructure_GetConfigValueTest_TestAction
            // 
            infrastructure_GetConfigValueTest_TestAction.Conditions.Add(scalarValueCondition1);
            resources.ApplyResources(infrastructure_GetConfigValueTest_TestAction, "infrastructure_GetConfigValueTest_TestAction");
            // 
            // scalarValueCondition1
            // 
            scalarValueCondition1.ColumnNumber = 1;
            scalarValueCondition1.Enabled = true;
            scalarValueCondition1.ExpectedValue = "1";
            scalarValueCondition1.Name = "scalarValueCondition1";
            scalarValueCondition1.NullExpected = false;
            scalarValueCondition1.ResultSet = 1;
            scalarValueCondition1.RowNumber = 1;
            // 
            // logging_AddEventLogTest_TestAction
            // 
            logging_AddEventLogTest_TestAction.Conditions.Add(checksumCondition2);
            resources.ApplyResources(logging_AddEventLogTest_TestAction, "logging_AddEventLogTest_TestAction");
            // 
            // checksumCondition2
            // 
            checksumCondition2.Checksum = "-1055356949";
            checksumCondition2.Enabled = true;
            checksumCondition2.Name = "checksumCondition2";
            // 
            // logging_ArchiveEventLogTest_TestAction
            // 
            logging_ArchiveEventLogTest_TestAction.Conditions.Add(checksumCondition3);
            resources.ApplyResources(logging_ArchiveEventLogTest_TestAction, "logging_ArchiveEventLogTest_TestAction");
            // 
            // checksumCondition3
            // 
            checksumCondition3.Checksum = "-1210831329";
            checksumCondition3.Enabled = true;
            checksumCondition3.Name = "checksumCondition3";
            // 
            // infrastructure_CheckDBInstanceExistsTest_TestAction
            // 
            infrastructure_CheckDBInstanceExistsTest_TestAction.Conditions.Add(scalarValueCondition2);
            resources.ApplyResources(infrastructure_CheckDBInstanceExistsTest_TestAction, "infrastructure_CheckDBInstanceExistsTest_TestAction");
            // 
            // scalarValueCondition2
            // 
            scalarValueCondition2.ColumnNumber = 1;
            scalarValueCondition2.Enabled = true;
            scalarValueCondition2.ExpectedValue = "1";
            scalarValueCondition2.Name = "scalarValueCondition2";
            scalarValueCondition2.NullExpected = false;
            scalarValueCondition2.ResultSet = 1;
            scalarValueCondition2.RowNumber = 1;
            // 
            // permissions_CheckForDBSchemaUserPermissionsTest_TestAction
            // 
            permissions_CheckForDBSchemaUserPermissionsTest_TestAction.Conditions.Add(scalarValueCondition4);
            resources.ApplyResources(permissions_CheckForDBSchemaUserPermissionsTest_TestAction, "permissions_CheckForDBSchemaUserPermissionsTest_TestAction");
            // 
            // scalarValueCondition4
            // 
            scalarValueCondition4.ColumnNumber = 1;
            scalarValueCondition4.Enabled = true;
            scalarValueCondition4.ExpectedValue = "1";
            scalarValueCondition4.Name = "scalarValueCondition4";
            scalarValueCondition4.NullExpected = false;
            scalarValueCondition4.ResultSet = 1;
            scalarValueCondition4.RowNumber = 1;
            // 
            // permissions_CheckForDBUserRoleMembershipTest_TestAction
            // 
            permissions_CheckForDBUserRoleMembershipTest_TestAction.Conditions.Add(scalarValueCondition5);
            resources.ApplyResources(permissions_CheckForDBUserRoleMembershipTest_TestAction, "permissions_CheckForDBUserRoleMembershipTest_TestAction");
            // 
            // scalarValueCondition5
            // 
            scalarValueCondition5.ColumnNumber = 1;
            scalarValueCondition5.Enabled = true;
            scalarValueCondition5.ExpectedValue = "1";
            scalarValueCondition5.Name = "scalarValueCondition5";
            scalarValueCondition5.NullExpected = false;
            scalarValueCondition5.ResultSet = 1;
            scalarValueCondition5.RowNumber = 1;
            // 
            // infrastructure_GetConfigValueTestData
            // 
            this.infrastructure_GetConfigValueTestData.PosttestAction = null;
            this.infrastructure_GetConfigValueTestData.PretestAction = null;
            this.infrastructure_GetConfigValueTestData.TestAction = infrastructure_GetConfigValueTest_TestAction;
            // 
            // logging_AddEventLogTestData
            // 
            this.logging_AddEventLogTestData.PosttestAction = null;
            this.logging_AddEventLogTestData.PretestAction = null;
            this.logging_AddEventLogTestData.TestAction = logging_AddEventLogTest_TestAction;
            // 
            // logging_ArchiveEventLogTestData
            // 
            this.logging_ArchiveEventLogTestData.PosttestAction = null;
            this.logging_ArchiveEventLogTestData.PretestAction = null;
            this.logging_ArchiveEventLogTestData.TestAction = logging_ArchiveEventLogTest_TestAction;
            // 
            // infrastructure_CheckDBInstanceExistsTestData
            // 
            this.infrastructure_CheckDBInstanceExistsTestData.PosttestAction = null;
            this.infrastructure_CheckDBInstanceExistsTestData.PretestAction = null;
            this.infrastructure_CheckDBInstanceExistsTestData.TestAction = infrastructure_CheckDBInstanceExistsTest_TestAction;
            // 
            // permissions_CheckForDBSchemaUserPermissionsTestData
            // 
            this.permissions_CheckForDBSchemaUserPermissionsTestData.PosttestAction = null;
            this.permissions_CheckForDBSchemaUserPermissionsTestData.PretestAction = null;
            this.permissions_CheckForDBSchemaUserPermissionsTestData.TestAction = permissions_CheckForDBSchemaUserPermissionsTest_TestAction;
            // 
            // permissions_CheckForDBUserRoleMembershipTestData
            // 
            this.permissions_CheckForDBUserRoleMembershipTestData.PosttestAction = null;
            this.permissions_CheckForDBUserRoleMembershipTestData.PretestAction = null;
            this.permissions_CheckForDBUserRoleMembershipTestData.TestAction = permissions_CheckForDBUserRoleMembershipTest_TestAction;
        }

        #endregion


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        #endregion


        [TestMethod()]
        public void infrastructure_GetConfigValueTest()
        {
            SqlDatabaseTestActions testActions = this.infrastructure_GetConfigValueTestData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        [TestMethod()]
        public void logging_AddEventLogTest()
        {
            SqlDatabaseTestActions testActions = this.logging_AddEventLogTestData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        [TestMethod()]
        public void logging_ArchiveEventLogTest()
        {
            SqlDatabaseTestActions testActions = this.logging_ArchiveEventLogTestData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }
        [TestMethod()]
        public void infrastructure_CheckDBInstanceExistsTest()
        {
            SqlDatabaseTestActions testActions = this.infrastructure_CheckDBInstanceExistsTestData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        [TestMethod()]
        public void permissions_CheckForDBSchemaUserPermissionsTest()
        {
            SqlDatabaseTestActions testActions = this.permissions_CheckForDBSchemaUserPermissionsTestData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }

        [TestMethod()]
        public void permissions_CheckForDBUserRoleMembershipTest()
        {
            SqlDatabaseTestActions testActions = this.permissions_CheckForDBUserRoleMembershipTestData;
            // Execute the pre-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PretestAction != null), "Executing pre-test script...");
            SqlExecutionResult[] pretestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PretestAction);
            // Execute the test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.TestAction != null), "Executing test script...");
            SqlExecutionResult[] testResults = TestService.Execute(this.ExecutionContext, this.PrivilegedContext, testActions.TestAction);
            // Execute the post-test script
            // 
            System.Diagnostics.Trace.WriteLineIf((testActions.PosttestAction != null), "Executing post-test script...");
            SqlExecutionResult[] posttestResults = TestService.Execute(this.PrivilegedContext, this.PrivilegedContext, testActions.PosttestAction);
        }


private SqlDatabaseTestActions infrastructure_GetConfigValueTestData;
private SqlDatabaseTestActions logging_AddEventLogTestData;
private SqlDatabaseTestActions logging_ArchiveEventLogTestData;
private SqlDatabaseTestActions infrastructure_CheckDBInstanceExistsTestData;
private SqlDatabaseTestActions permissions_CheckForDBSchemaUserPermissionsTestData;
private SqlDatabaseTestActions permissions_CheckForDBUserRoleMembershipTestData;
    }
}
