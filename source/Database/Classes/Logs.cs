using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DBRestoreClr
{
    [XmlRoot("Logs")]
    [XmlSerializerAssembly("DBRestoreClr.XmlSerializers, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")]
    public class Logs
    {
        private List<Log> _logs = new List<Log>();

        [XmlElement("Log")]
        public List<Log> Log
        {
            get { return _logs; }
        }

        public void AddMessage(string evt, string type, string instanceName, string databaseName, string message)
        {
            var log = new Log() { Event = evt, Type = type, InstanceName = instanceName, DatabaseName = databaseName };
            log.SetMessage(message);
            _logs.Add(log);
        }

        public string GetXml()
        {
            var serializer = new XmlSerializer(typeof(Logs));
            var writer = new StringWriter();
            serializer.Serialize(writer, this);
            _logs = new List<Log>();
            return writer.ToString();
        }
    }

    public class Log
    {
        private string _message;

        public void SetMessage(string message)
        {
            _message = message;
        }

        [XmlAttribute("Event")]
        public string Event { get; set; }

        [XmlAttribute("Type")]
        public string Type { get; set; }

        [XmlAttribute("InstanceName")]
        public string InstanceName { get; set; }

        [XmlAttribute("DatabaseName")]
        public string DatabaseName { get; set; }

        [XmlAnyElement]
        public XmlCDataSection Message
        {
            get
            {
                var doc = new XmlDocument();
                return doc.CreateCDataSection(_message);
            }
            set
            {
                _message = value.Value;
            }
        }
    }
}
