using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.SqlServer.Server;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DBRestoreClr
{
    public class Common
    {
        private string _processName;
        private bool _verbose;
        private Logs _logs;
        private string _instanceName;
        private string _databaseName;
        private int _timeout = 6000;

        public int Timeout
        {
            set { _timeout = value; } 
        }

        public Common(string processName, string instanceName, string databaseName, bool Verbose)
        {
            _processName = processName;
            _instanceName = instanceName;
            _databaseName = databaseName;
            _logs = new Logs();
            _verbose = Verbose;
        }

        private string GetConnStr(bool useInst)
        {
            if (useInst)
            {
                return string.Format("Data Source={0};Initial Catalog=master;Integrated Security=True;Pooling=False;connect timeout={1};",
                                     _instanceName, _timeout);
            }

            return "context connection=true";
        }

        public DataSet ExecuteSqlReturnData(string sql, bool useInstance)
        {
            var dataSet = new DataSet();

            try
            {
                using (var sqlConn = OpenConnection(useInstance))
                {
                    if (_verbose)
                    {
                        sqlConn.InfoMessage += SqlConnInfoMessage;
                        AddLog("Information", string.Format("Sql: {0}", sql), true);
                    }

                    using (SqlDataAdapter sqlAdapter = new SqlDataAdapter(sql, sqlConn))
                    {
                        sqlAdapter.SelectCommand.CommandTimeout= _timeout;
                        sqlAdapter.Fill(dataSet);
                    }
                    return dataSet;
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "Failed executing sql with return data", false);
                AddLog("Error", string.Format("Sql: {0}", sql), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return null;
            }
        }

        public void SqlConnInfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            try
            {
                AddLog("Information", string.Format("Message: {0}", e.Message), true);
            }
            catch (Exception ex)
            {
                AddLog("Error", "SqlConnInfoMessage: Failed", false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
            }
        }

        public bool CheckInstance(string instanceName, bool checkConnection)
        {
            var bOK = false;
            try
            {
                if (_verbose)
                {
                    AddLog("Information", "Checking instance", false);
                    AddLog("Information", string.Format("Instance: {0}", instanceName), false);
                    AddLog("Information", string.Format("CheckConnection: {0}", checkConnection), true);
                }

                var sql = string.Format("SELECT 1 FROM infrastructure.DatabaseInstance WHERE  InstanceName = '{0}'", instanceName);

                var exists = ConvertFromDBVal<int>(ExecuteSqlScalar(sql, false, false));

                if (exists == 1)
                {
                    bOK = true;
                }

                if (checkConnection)
                {
                    sql = string.Format("SELECT	1");

                    exists = ConvertFromDBVal<int>(ExecuteSqlScalar(sql, true, false));

                    if (exists == 1)
                    {
                        bOK = true;
                    }
                    else
                    {
                        bOK = false;
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "CheckInstance: Failed", false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }
            return bOK;
        }

        public bool ExecuteSql(string sql, bool useInstance, bool useMaster)
        {
            var logs = new string[1];
            try
            {
                if (_verbose)
                {
                    AddLog("Information", string.Format("Started Executing Sql: {0}", sql), true);
                }

                using (var sqlConn = OpenConnection(useInstance))
                {
                    if (_verbose)
                    {
                        sqlConn.InfoMessage += SqlConnInfoMessage;
                    }

                    if (useInstance)
                    {
                        ChangeDatabase(useMaster, sqlConn);
                    }

                    using (SqlCommand sqlCmd = new SqlCommand(sql, sqlConn))
                    {
                        sqlCmd.CommandTimeout = _timeout;
                        sqlCmd.CommandType = CommandType.Text;
                        sqlCmd.ExecuteNonQuery();
                    }
                }
                if (_verbose)
                {
                    AddLog("Information", string.Format("Finished Executing Sql: {0}", sql), true);
                }
                return true;
            }
            catch (Exception ex)
            {
                AddLog("Error", "Failed executing sql", false);
                AddLog("Error", string.Format("Sql: {0}", sql), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return false;
            }
        }

        public object ExecuteSqlScalar(string sql, bool useInstance, bool useMaster)
        {
            var logs = new string[1];
            try
            {
                if (_verbose)
                {
                    AddLog("Information", string.Format("Started Executing Sql: {0}", sql), true);
                }

                using (var sqlConn = OpenConnection(useInstance))
                {
                    if (_verbose)
                    {
                        sqlConn.InfoMessage += SqlConnInfoMessage;
                    }

                    if (useInstance)
                    {
                        ChangeDatabase(useMaster, sqlConn);
                    }

                    using (SqlCommand sqlCmd = new SqlCommand(sql, sqlConn))
                    {
                        sqlCmd.CommandTimeout = _timeout;
                        sqlCmd.CommandType = CommandType.Text;
                        return sqlCmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "Failed executing sql with scalar return", false);
                AddLog("Error", string.Format("Sql: {0}", sql), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return null;
            }
        }

        private SqlConnection ChangeDatabase(bool useMaster, SqlConnection sqlConn)
        {
            if (useMaster)
            {
                sqlConn.ChangeDatabase("master");
            }
            else if (_databaseName != null)
            {
                if (_verbose)
                {
                    AddLog("Information", string.Format("Changing database to {0}", _databaseName), true);
                }

                sqlConn.ChangeDatabase(_databaseName);
            }

            return sqlConn;
        }
        
        private List<FileInfo> GetFiles(string dir)
        {
            List<FileInfo> files = new List<FileInfo>();

            try
            {
                if (_verbose)
                {
                    AddLog("Information", string.Format("Getting files in: {0}", dir), true);
                }

                var dirInfo = new DirectoryInfo(dir);

                foreach (FileInfo fi in dirInfo.GetFiles("*.sql"))
                {
                    if (fi.Extension == ".sql")
                    {
                        files.Add(fi);
                    }
                }

                foreach (var directory in dirInfo.GetDirectories())
                {
                    files.AddRange(GetFiles(directory.FullName));
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "GetFiles: Failed getting files", false);
                AddLog("Error", string.Format("Dir: {0}", dir), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return null;
            }

            return files;
        }

        public bool RunScripts(string dir)
        {
            var bRet = false;

            try
            {
                List<FileInfo> scripts = GetFiles(dir);

                if (_verbose)
                {
                    AddLog("Information", string.Format("File count: {0}", scripts.Count), true);
                }

                foreach (var script in scripts)
                {
                    bRet = RunScript(script.FullName);
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "RunScripts: Failed Running Scripts", false);
                AddLog("Error", string.Format("Dir: {0}", dir), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return false;
            }

            return bRet;
        }

        private bool RunScript(string script)
        {
            var bOk = false;
            var sqlScriptSplitRegEx = new Regex(@"^\s*GO\s*$", RegexOptions.IgnoreCase | RegexOptions.Multiline |
                                                               RegexOptions.Compiled);
            try
            {
                if (_verbose)
                {
                    AddLog("Information", string.Format("Executing script: {0}...", script), true);
                }

                var preScript = GetFileContents(script);

                if (!String.IsNullOrEmpty(preScript))
                {
                    foreach (var sql in sqlScriptSplitRegEx.Split(preScript))
                    {
                        if (!String.IsNullOrEmpty(sql))
                        {
                            bOk = ExecuteSql(sql, true, false);
                        }
                    }
                    if (bOk)
                    {
                        if (_verbose)
                        {
                            AddLog("Information", string.Format("Executing script: {0}...Completed", script), true);
                        }
                    }
                    else
                    {
                        if (_verbose)
                        {
                            AddLog("Error", string.Format("Executing script: {0}...Failed", script), true);
                        }
                    }
                }
                else
                {
                    bOk = true;
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "RunScripts: Failed Running Scripts", false);
                AddLog("Error", string.Format("Script: {0}", script), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return false;
            }

            return bOk;
        }

        private string GetFileContents(string script)
        {
            var sr = new StreamReader(script);

            try
            {
                return sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                AddLog("Error", "GetFileContents: failed", false);
                AddLog("Error", string.Format("Script: {0}", script), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return null;
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    sr = null;
                }
            }
        }

        public bool CheckForDBSchemaUserPermissions(string databaseUserName, string permission, string schemaName)
        {
            var bOK = false;

            try
            {
                if (_verbose)
                {
                    AddLog("Information", "Checking user permission", false);
                    AddLog("Information", string.Format("User: {0}", databaseUserName), false);
                    AddLog("Information", string.Format("Permission: {0}", permission), false);
                    AddLog("Information", string.Format("Schema: {0}", schemaName), true);
                }

                var sql = string.Format("SELECT	1 " +
                                        "FROM {0}.sys.database_permissions AS dp " +
                                        "INNER JOIN {0}.sys.database_principals AS dp2 ON grantee_principal_id = principal_id " +
                                        "INNER JOIN {0}.sys.schemas AS s ON s.schema_id = dp.major_id " +
                                        "WHERE dp.class_desc = 'SCHEMA' AND dp2.name = '{1}' " +
                                        "AND s.name = '{2}' AND dp.permission_name = '{3}'", _databaseName, databaseUserName, schemaName, permission);

                var exists = ConvertFromDBVal<int>(ExecuteSqlScalar(sql, true, true));

                if (exists == 1)
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "CheckForDBSchemaUserPermissions: Failed checking user permission", false);
                AddLog("Error", string.Format("User: {0}", databaseUserName), false);
                AddLog("Error", string.Format("Permission: {0}", permission), false);
                AddLog("Error", string.Format("Schema: {0}", schemaName), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }

        public string GetInstanceConfigValue(string config, bool raiseError)
        {
            try
            {
                if (_verbose)
                {
                    AddLog("Information", string.Format("Getting Config: {0}", config), true);
                }

                var sql = string.Format("SELECT	Value " +
                                        "FROM   restore.[Config] " +
                                        "WHERE  Config = {0}" +
                                        "AND    InstanceName = {1} " +
                                        "AND	DatabaseName = {2} ", config, _instanceName, _databaseName);

                return ConvertFromDBVal<string>(ExecuteSqlScalar(sql, false, false));

            }
            catch (Exception ex)
            {
                AddLog("Error", "GetInstanceConfigValue: Failed Getting config value", false);
                AddLog("Error", string.Format("Config: {0}", config), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return null;
            }
        }

        public void AddLog(string type, string message, bool persist)
        {
            _logs.AddMessage(_processName, type, _instanceName, _databaseName, message);

            if (persist)
            {
                PersistLogs();
            }
        }

        public void PersistLogs()
        {
            try
            {
                using (var sqlConn = OpenConnection(false))
                {
                    var sqlParam = new SqlParameter
                    {
                        Direction = ParameterDirection.Input,
                        SqlDbType = SqlDbType.Xml,
                        Value = _logs.GetXml(),
                        ParameterName = "@Log"
                    };

                    using (var sqlCmd = new SqlCommand())
                    {
                        sqlCmd.CommandTimeout = _timeout;
                        sqlCmd.CommandText = "logging.AddEventLog";
                        sqlCmd.CommandType = CommandType.StoredProcedure;
                        sqlCmd.Connection = sqlConn;
                        sqlCmd.Parameters.Add(sqlParam);
                        sqlCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                SqlContext.Pipe.Send(string.Format("Error: PersistLogs"));
                SqlContext.Pipe.Send(ex.ToString());
            }
        }

        private SqlConnection OpenConnection(bool useInstance)
        {
            var sqlConn = new SqlConnection(GetConnStr(useInstance));

            if (sqlConn.State != ConnectionState.Open)
            {
                sqlConn.Open();
            }

            return sqlConn;
        }

        public bool CheckForUser(string databaseUserName)
        {
            var bOK = false;

            try
            {
                if (_verbose)
                {
                    AddLog("Information", "Checking for user", false);
                    AddLog("Information", string.Format("User: {0}", databaseUserName), true);
                }

                var sql = string.Format("SELECT 1 FROM {0}.sys.sysusers WHERE name = '{1}'", _databaseName, databaseUserName);

                var exists = ConvertFromDBVal<int>(ExecuteSqlScalar(sql, true, true));

                if (exists == 1)
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "CheckForUser: Failed checking user exists", false);
                AddLog("Error", string.Format("Database: {0}", _databaseName), false);
                AddLog("Error", string.Format("User: {0}", databaseUserName), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }
        
        public bool CheckForDBUserRoleMembership(string databaseUserName, string roleName)
        {
            var bOK = false;

            try
            {
                if (_verbose)
                {
                    AddLog("Information", "Checking database user role exists", false);
                    AddLog("Information", string.Format("User: {0}", databaseUserName), false);
                    AddLog("Information", string.Format("Role: {0}", roleName), true);
                }

                var sql = string.Format("SELECT	1 " +
                                        "FROM ({0}.sys.database_role_members AS drm " +
                                        "INNER JOIN {0}.sys.database_principals AS dp ON drm.member_principal_id = dp.principal_id) " +
                                        "INNER JOIN {0}.sys.database_principals AS dpr ON drm.role_principal_id = dpr.principal_id " +
                                        "WHERE dp.name = '{1}' AND dpr.name = '{2}'", _databaseName, databaseUserName, roleName);

                var exists = ConvertFromDBVal<int>(ExecuteSqlScalar(sql, true, true));

                if (exists == 1)
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "CheckForDBUserRoleMembership: Failed checking database user role exists", false);
                AddLog("Error", string.Format("User: {0}", databaseUserName), false);
                AddLog("Error", string.Format("Role: {0}", databaseUserName), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }

        public bool CheckForLogin(string loginName)
        {
            var bOK = false;

            try
            {
                if (_verbose)
                {
                    AddLog("Information", "Checking for login", false);
                    AddLog("Information", string.Format("Login: {0}", loginName), true);
                }

                var sql = string.Format("SELECT 1 FROM master..syslogins WHERE name = '{0}'", loginName);

                var exists = ConvertFromDBVal<int>(ExecuteSqlScalar(sql, true, true));

                if(exists == 1)
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "CheckForLogin: Failed checking login exists", false);
                AddLog("Error", string.Format("Login: {0}", loginName), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }

        public bool DirectoryExists(string directory)
        {
            var bOK = false;

            try
            {
                if (_verbose)
                {
                    AddLog("Information", "Checking directory exists", false);
                    AddLog("Information", string.Format("Directory: {0}", directory), true);
                }

                var sql = string.Format("exec xp_cmdshell 'IF EXIST \"{0}\" ECHO 1'", directory);

                var exists = ConvertFromDBVal<string>(ExecuteSqlScalar(sql, true, true));
                    
                if ((exists != null) && (exists == "1"))
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "DirectoryExists: Failed checking directory exists", false);
                AddLog("Error", string.Format("Directory: {0}", directory), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }

        private T ConvertFromDBVal<T>(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return default(T); // returns the default value for the type
            }
            else
            {
                return (T)obj;
            }
        }

        public bool CreateDirectory(string directory)
        {
            var bOK = false;

            try
            {
                if (_verbose)
                {
                    AddLog("Information", "Checking directory exists", false);
                    AddLog("Information", string.Format("Directory: {0}", directory), true);
                }

                var sql = string.Format("exec xp_cmdshell 'MKDIR \"{0}\"'", directory);

                if (ExecuteSql(sql, true, true))
                {
                    if (DirectoryExists(directory))
                    {
                        bOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                AddLog("Error", "DirectoryExists: Failed checking directory exists", false);
                AddLog("Error", string.Format("Directory: {0}", directory), false);
                AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }
    }
}