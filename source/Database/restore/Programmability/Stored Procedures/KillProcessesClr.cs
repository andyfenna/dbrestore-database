using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;
using System.Threading;

namespace DBRestoreClr
{
    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Kills all processes on a database instance.
        /// </summary>
        /// <param name="InstanceName">Instance to kill processes on.</param>
        /// <param name="DatabaseName">Database to kill processes on.</param>
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "KillProcessesClr")]
        public static int KillProcessesClr(string InstanceName, string DatabaseName, bool Verbose)
        {
            try
            {
                _common = new Common("KillProcesses", InstanceName, DatabaseName, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information", "Started killing processes", true);

                    if (KillProcesses(GetProcesses(DatabaseName)))
                    {
                        _common.AddLog("Success", "Finished killing processes", true);
                        return 1;
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed killing processes", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool KillProcesses(DataSet processes)
        {
            var bRet = false;

            try
            {
                if (processes != null)
                {
                    _common.AddLog("Information", string.Format("{0} processes to kill.", processes.Tables[0].Rows.Count), true);
                    
                    if (processes.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow process in processes.Tables[0].Rows)
                        {
                            bRet = KillProcess(process["spid"].ToString(), process["loginame"].ToString());
                            if (!bRet)
                            {
                                break;
                            }
                        }
                    }
                    bRet = true;
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed killing processes", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bRet = false;
            }
            return bRet;
        }

        private static bool KillProcess(string spid, string login)
        {
            var bRet = false;

            try
            {
                _common.AddLog("Information", string.Format("About to KILL {0} process, with login {1}.", spid, login), true);
                
                if (CheckProcessExists(spid))
                {
                    var sql = string.Format("KILL {0}", spid);

                    if (_common.ExecuteSql(sql, true, true))
                    {
                        return WaitForProcessToKill(spid);
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", string.Format("Failed killing process {0}, with login {1}", spid, login), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bRet = false;
            }
            return bRet;
        }

        private static bool WaitForProcessToKill(string spid)
        {
            var bRet = false;

            try
            {
                _common.AddLog("Information", string.Format("Waiting for process {0} to kill.", spid), true);

                do
                {
                    Thread.Sleep(10000);
                } while (!CheckProcessExists(spid));

                _common.AddLog("Information",  string.Format("Process {0} has been killed.", spid), true);
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", string.Format("Failed waiting for process to kill {0}", spid), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
            }
            return bRet;
        }

        private static bool CheckProcessExists(string spid)
        {
            var bRet = false;

            try
            {
                _common.AddLog("Information", string.Format("Checking that process {0} exists.", spid), true);

                var sql = string.Format("SELECT 1 " +
                                        "FROM master..sysprocesses " +
                                        "WHERE spid = {0}", spid);

                var exists = _common.ExecuteSqlScalar(sql, true, true);

                if ((int) exists == 1)
                {
                    _common.AddLog("Information", string.Format("Process {0} exists", spid), true);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", string.Format("Failed checking process {0} exists", spid), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bRet = false;
            }
            return bRet;
        }

        private static DataSet GetProcesses(string databaseName)
        {
            try
            {
                var sql = string.Format("SELECT MIN(spid) AS spid, loginame, COUNT(*) as cnt " +
                                        "FROM sysprocesses " +
                                        "WHERE dbid = DB_ID('{0}') " +
                                        "AND spid > 50 " +
                                        "AND spid != @@SPID " +
                                        "AND dbid NOT IN (1,2,3,4) " +
                                        "GROUP BY loginame", databaseName);

                return _common.ExecuteSqlReturnData(sql, true);

            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed getting processe", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return null;
            }
        }
    }
}