using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;
using System.Threading;

namespace DBRestoreClr
{
    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Restricts\Unrestricts access to a database instance.
        /// </summary>
        /// <param name="InstanceName">Instance to Restricts\Unrestricts access on.</param>
        /// <param name="DatabaseName">Database to Restricts\Unrestricts access on.</param>
        /// <param name="Restrict">1=Restrict, 0 = Unrestrict</param>
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "RestrictAccessClr")]
        public static int RestrictAccessClr(string InstanceName, string DatabaseName, bool Restrict, bool Verbose)
        {
            try
            {
                _common = new Common("RestrictAccess", InstanceName, DatabaseName, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information", string.Format("Started {0} access", (Restrict ? "restricting" : "unrestricting")), true);

                    if (RestrictAccess(DatabaseName, Restrict))
                    {
                        _common.AddLog("Success", string.Format("Finished {0} access", (Restrict ? "restricting" : "unrestricting")), true);
                        return 1;
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", string.Format("Failed {0} access", (Restrict ? "restricting" : "unrestricting")), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool RestrictAccess(string databaseName, bool restrict)
        {
            var bOK = false;
            try
            {
                var sql = "";

                if (restrict)
                {
                    sql = string.Format("ALTER DATABASE {0} SET RESTRICTED_USER WITH ROLLBACK IMMEDIATE", databaseName);
                }
                else
                {
                    sql = string.Format("ALTER DATABASE {0} SET MULTI_USER", databaseName);
                }

                _common.ExecuteSql(sql, true, true);

                if (CheckAccess(databaseName, restrict))
                {
                    _common.AddLog("Information", string.Format("Access is {0}", (restrict ? "restricted" : "unrestricted")), true);
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", string.Format("Failed {0} access", (restrict ? "restricting" : "unrestricting")), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }
            return bOK;
        }

        private static bool CheckAccess(string databaseName, bool restrict)
        {
            var bOK = false;
            try
            {
                _common.AddLog("Information", string.Format("Checking {0} access.", (restrict ? "restricted" : "unrestricted")), true);

                var sql = string.Format("SELECT user_access_desc FROM sys.databases WHERE name ='{0}'", databaseName);

                string access = (string) _common.ExecuteSqlScalar(sql, true, true);

                if ((restrict) && (access == "RESTRICTED_USER"))
                {
                    return true;
                }
                else if ((!restrict) && (access == "MULTI_USER"))
                {
                    return true;
                }
                bOK = false;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Checking access failed", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }
            return bOK;
        }
    }
}