﻿-- =============================================
-- Author:		Andrew Fenna
-- Create date: 2012-07-07
-- Description:	This procedure kills database processes
-- Modification History:
-- 18-10-2012 - AJF addded 10 sec delay between kills
-- =============================================
CREATE PROCEDURE [restore].[KillProcesses] 
    @InstanceName AS NVARCHAR(50),
	@DatabaseName SYSNAME
AS 
    BEGIN
  
        SET XACT_ABORT ON
        SET NOCOUNT ON
		DECLARE @Event VARCHAR(50) = 'KillProcesses'

        BEGIN TRY        
			DECLARE @spid INT ,
					@cnt INT ,
					@login VARCHAR(25), 
					@sql VARCHAR(255) ,
					@TSQL AS NVARCHAR(MAX),
					@Log XML
                
			SET @Log = 
			(
				SELECT @Event as "@Event",
					   'Information' as "@Type",
					   'Started killing processes.' as "text()"
				FOR XML PATH('Log'), ROOT('Logs')
			)
			EXEC [logging].[AddEventLog] @Log;

			SET @TSQL = 
			'SELECT @spid = a.spid, @login = a.loginame, @cnt = a.cnt
			FROM  
			OPENROWSET(''SQLNCLI'', 
					   ''Server=' + @InstanceName +';Trusted_Connection=yes; Integrated Security=SSPI'', 
					   ''SELECT MIN(spid) AS spid, loginame, COUNT(*) as cnt
						FROM    sysprocesses			 
						WHERE   dbid = DB_ID(''''' + @DatabaseName + ''''')
								AND spid > 50
								AND dbid NOT IN (1,2,3,4)
								AND dbid NOT IN (1,2,3,4)
								GROUP BY loginame'') as a'

			EXEC sp_executesql @query = @TSQL,
				@params = N'@spid int OUTPUT, @login VARCHAR(25) OUTPUT, @cnt int OUTPUT',
				@spid = @spid OUTPUT,
				@login = @login OUTPUT,	
				@cnt = @cnt OUTPUT;

            WHILE @spid IS NOT NULL 
                BEGIN 

					SET @Log = 
					(
						SELECT @Event as "@Event",
								'Warning' as "@Type",
								'About to KILL ' + RTRIM(@spid) +  ' process, with login ' + RTRIM(@login) as "text()"
						FOR XML PATH('Log'), ROOT('Logs')
					)
					EXEC [logging].[AddEventLog] @Log;

                    DECLARE @exists int = 0

					SET @TSQL = 'SELECT @exists = a.[exists]
								FROM  
								OPENROWSET(''SQLNCLI'', 
											''Server=' + @InstanceName
								+ ';Trusted_Connection=yes; Integrated Security=SSPI'', 
											''SELECT 1 as [exists]
											FROM    sysprocesses			 
											WHERE spid=' + CONVERT(VARCHAR(10),@spid) + ''') as a'

							EXEC sp_executesql @query = @TSQL, 
								@params = N'@exists int OUTPUT',
								@exists = @exists OUTPUT;


					IF (@exists) = 1
						BEGIN
							SET @sql = 'KILL ' + RTRIM(@spid) 
							EXEC(@sql)  
						END 

					WHILE @exists = 1
						BEGIN
							
							--WAIT FOR 10 SECS 
							WAITFOR DELAY '00:00:10'

							SELECT  @exists = count(*)
							FROM    master..sysprocesses
							WHERE   dbid = DB_ID(@DatabaseName)
									AND spid = @spid

							SET @Log = 
							(
								SELECT @Event as "@Event",
										'Information' as "@Type",
										'Waiting for process ' + RTRIM(@spid) + ' to KILL, with login ' + RTRIM(@login) as "text()"
								FOR XML PATH('Log'), ROOT('Logs')
							)
							EXEC [logging].[AddEventLog] @Log;
						END

                    SELECT  @spid = MIN(spid) ,
							@login = loginame,
                            @cnt = COUNT(*) 
                    FROM    master..sysprocesses
                    WHERE   dbid = DB_ID(@DatabaseName)
                            AND spid != @@SPID  
							AND spid > 50
							AND dbid NOT IN (1,2,3,4)
					GROUP BY loginame;
					
					SET @Log = 
					(
						SELECT @Event as "@Event",
								'Warning' as "@Type",
								RTRIM(@cnt) + ' processes remain.' as "text()"
						FOR XML PATH('Log'), ROOT('Logs')
					)
					EXEC [logging].[AddEventLog] @Log;
                END 

			SET @Log = 
			(
				SELECT @Event as "@Event",
						'Success' as "@Type",
						'Finished killing processes.' as "text()"
				FOR XML PATH('Log'), ROOT('Logs')
			)
			EXEC [logging].[AddEventLog] @Log;

			IF(@cnt) IS NULL
				BEGIN
					RETURN 1			
				END
			ELSE
				BEGIN
					RETURN 0
				END    
        END TRY      

        BEGIN CATCH		     

			DECLARE @errorLog XML = 
			(
				SELECT (	
					SELECT "@Event", "@Type", "text()" FROM (

					SELECT  'RestoreDatabase' as "@Event",
							'Error' as "@Type",
							'Failed killing processes.' as "text()"
					UNION ALL
					SELECT 'RestoreDatabase' as "@Event",
							'Error' as "@Type",
							'Error: '
							+ CONVERT(NVARCHAR(50), ERROR_NUMBER())
							+ ', Severity '
							+ CONVERT(NVARCHAR(5), ERROR_SEVERITY())
							+ ', State '
							+ CONVERT(NVARCHAR(5), ERROR_STATE())
							+ ', Procedure ' 
							+ ISNULL(ERROR_PROCEDURE(),'-') 
							+ ', Line ' 
							+ CONVERT(NVARCHAR(5), ERROR_LINE()) 
							+ ', Message ' 
							+ CONVERT(NVARCHAR(MAX), ERROR_MESSAGE()) as "text()"
					) A
				FOR XML PATH ('Log'), ROOT('Logs')
				) As XmlOutput
			)

			EXEC [logging].[AddEventLog] @errorLog;
            RETURN 0
        END CATCH 
    END