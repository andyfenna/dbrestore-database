﻿-- =============================================
-- Author:		Andrew Fenna
-- Create date: 2013-02-17
-- Description:	This sproc will return the configuration value for the instance\database
-- Modification History:
-- =============================================
CREATE PROCEDURE [restore].[GetConfigValue]
    @Config NVARCHAR(50) = NULL ,
    @InstanceName AS SYSNAME ,
    @DatabaseName AS SYSNAME ,
	@RaiseError AS BIT = 1,
    @Value NVARCHAR(MAX) OUTPUT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	SET XACT_ABORT ON;
	SET @Value = NULL;

	DECLARE @Event VARCHAR(50) = 'GetConfigValue';

	BEGIN TRY
	
		SELECT  @Value = Value
		FROM    [restore].[Config] (NOLOCK)
		WHERE   Config = @Config
		AND		InstanceName = @InstanceName
		AND		DatabaseName = @DatabaseName

		IF(@Value) IS NULL
			BEGIN
				IF(@RaiseError) = 1
					BEGIN          
						RAISERROR ('Configuration value does not exist.', 16,1);
					END                  
			END

		RETURN 1

	END TRY

	BEGIN CATCH		
			DECLARE @errorLog XML = 
			(
				SELECT (	
					SELECT "@Event", "@Type", "text()" FROM (

					SELECT  @Event as "@Event",
							'Error' as "@Type",
							'Failed adding event log.' as "text()"
					UNION ALL
					SELECT @Event as "@Event",
							'Error' as "@Type",
							'Error: '
							+ CONVERT(NVARCHAR(50), ERROR_NUMBER())
							+ ', Severity '
							+ CONVERT(NVARCHAR(5), ERROR_SEVERITY())
							+ ', State '
							+ CONVERT(NVARCHAR(5), ERROR_STATE())
							+ ', Procedure ' 
							+ ISNULL(ERROR_PROCEDURE(),'-') 
							+ ', Line ' 
							+ CONVERT(NVARCHAR(5), ERROR_LINE()) 
							+ ', Message ' 
							+ CONVERT(NVARCHAR(MAX), ERROR_MESSAGE()) as "text()"
					) A
				FOR XML PATH ('Log'), ROOT('Logs')
				) As XmlOutput
			)

			EXEC [logging].[AddEventLog] @errorLog;

			IF XACT_STATE() <> 0 
				BEGIN	
					ROLLBACK TRANSACTION															
				END		
			RETURN 0
	END CATCH 
END

