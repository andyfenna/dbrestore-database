using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;


namespace DBRestoreClr
{

    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Run scripts to a database instance.
        /// </summary>
        /// <param name="InstanceName">Instance to run scritps on.</param>    
        /// <param name="DatabaseName">Database to run scripts on.</param>
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "RunScriptsClr")]
        public static int RunScriptsClr(string InstanceName, string DatabaseName, bool Verbose)
        {
            try
            {
                _common = new Common("RunScripts", InstanceName, DatabaseName, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information", "Started running scripts", true);

                    var snv = _common.GetInstanceConfigValue("SVNExe", true);
                    var usr = _common.GetInstanceConfigValue("SVNUsr", true);
                    var pwd = _common.GetInstanceConfigValue("SVNPwd", true);
                    var checkOutUrl = _common.GetInstanceConfigValue("SVNCheckoutUrl", true);
                    var toDir = _common.GetInstanceConfigValue("SVNToDir", true);


                    if (Verbose)
                    {
                        _common.AddLog("Information", "Parameters:", false);
                        _common.AddLog("Information", string.Format("SVNExe {0}.", snv), false);
                        _common.AddLog("Information", string.Format("SVNUsr {0}.", usr), false);
                        _common.AddLog("Information", string.Format("SVNPwd {0}.", pwd), true);
                    }

                    if (CheckOutScripts(checkOutUrl, toDir, snv, usr, pwd))
                    {
                        if (_common.RunScripts(toDir))
                        {
                            _common.AddLog("Success", "Finished running scripts", true);
                            return 1;
                        }
                    }

                    return 0;

                }
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed running scripts", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool CheckOutScripts(string checkOutUrl, string toDir, string snv, string usr, string pwd)
        {
            var bOK = false;

            try
            {
                var args = "";

                if(File.Exists(snv))
                {
                    if (!Directory.Exists(toDir))
                    {
                        _common.AddLog("Information", string.Format("Created directory {0}", toDir), true);
                        Directory.CreateDirectory(toDir);
                    }

                    if (Directory.Exists(toDir + "//.svn"))
                    {
                        args = string.Format("update {0}", toDir);
                    }
                    else
                    {
                        args = string.Format("checkout {0} {1}", checkOutUrl, toDir);
                    }

                    args = string.Format(args + " --username {0} --password {1}", usr, pwd);

                    using (Process proc = new Process())
                    {
                        proc.EnableRaisingEvents = false;
                        proc.StartInfo.FileName = snv;
                        proc.StartInfo.Arguments = args;
                        proc.StartInfo.UseShellExecute = false;
                        proc.StartInfo.RedirectStandardOutput = true;
                        proc.Start();

                        while (!proc.StandardOutput.EndOfStream)
                        {
                            _common.AddLog("Information", proc.StandardOutput.ReadLine(), true);
                        }
                    }
                    bOK = true;
                }
                else
                {
                    _common.AddLog("Error", string.Format("SVNExe {0} does not exist", snv), true);
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed checking out scripts", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }
            return bOK;
        }
    }
}