using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;

namespace DBRestoreClr
{

    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Restores a database to a database instance.
        /// </summary>
        /// <param name="InstanceName">Instance to restore database on.</param>
        /// <param name="DatabaseName">Database to restore.</param>
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "RestoreDatabaseClr")]
        public static int RestoreDatabaseClr(string InstanceName, string DatabaseName, bool Verbose)
        {
            try
            {
                _common = new Common("RestoreDatabase", InstanceName, DatabaseName, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information", "Started restoring", true);

                    var preRestoreDate = GetRestoreDate(DatabaseName);

                    //get configs
                    var backup = _common.GetInstanceConfigValue("Backup", true);
                    var backupType = _common.GetInstanceConfigValue("BackupType", true);
                    var dataFiles = _common.GetInstanceConfigValue("DataFiles", true);
                    var logFiles = _common.GetInstanceConfigValue("LogFiles", true);
                    var maxTransferSize = _common.GetInstanceConfigValue("MaxTransferSize", true);

                    if (Verbose)
                    {
                        _common.AddLog("Information", "Parameters:", false );
                        _common.AddLog("Information", string.Format("Database: {0}", DatabaseName), false);
                        _common.AddLog("Information", string.Format("BackupType: {0}", backupType), false);
                        _common.AddLog("Information", string.Format("Backup: {0}", backup), false);
                        _common.AddLog("Information", string.Format("DataFiles: {0}", dataFiles), false);
                        _common.AddLog("Information", string.Format("LogFiles: {0}", logFiles), false);
                        _common.AddLog("Information", string.Format("Pre restore date {0}", preRestoreDate), true);
                    }

                    if (CreateDirectories(dataFiles, logFiles, Verbose))
                    {
                        _common.Timeout = 0;

                        if (ExecuteRestore(DatabaseName, backup, dataFiles, logFiles, backupType, Verbose, maxTransferSize))
                        {
                            _common.Timeout = 180;

                            var postRestoreDate = GetRestoreDate(DatabaseName);

                            if (Verbose)
                            {
                                _common.AddLog("Information", string.Format("Post restore date {0}", postRestoreDate), true);
                            }

                            if (preRestoreDate != null)
                            {
                                if (DateTime.Parse(postRestoreDate) > DateTime.Parse(preRestoreDate))
                                {
                                    _common.AddLog("Success", "Finished restoring", true);
                                    return 1;
                                }

                                _common.AddLog("Warning", "Finished restoring, however the current restore date is greater than the previous restore date.", true);
                                return 1;
                            }

                            _common.AddLog("Success", "Finished restoring", true);

                            return 1;
                        }

                        return 0;
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed restoring", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool ExecuteRestore(string databaseName, string backup, string dataFiles, string logFiles, string backupType, bool verbose, string maxTransferSize)
        {
            var bOk = false;

            try
            {
                if (backupType == "rg")
                {
                    bOk = ExecuteRedGateRestore(GetRestoreSql(databaseName, backup, dataFiles, logFiles, backupType, maxTransferSize), verbose);
                }
                else
                {
                    bOk = _common.ExecuteSql(GetRestoreSql(databaseName, backup, dataFiles, logFiles, backupType, maxTransferSize), true, true);
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed executing restore", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOk = false;
            }

            return bOk;
        }

        private static bool IsDirectory(string backup)
        {
            var bOk = false;

            try
            {
                var attr = File.GetAttributes(backup);

                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    bOk = true;
                }
                else
                {
                    bOk = false;
                }
            }
            catch (Exception ex )
            {
                _common.AddLog("Error", "Failed executing restore", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOk = false;
            }
            return bOk;
        }
        
        private static string GetRestoreSql(string databaseName, string backup, string dataFiles, string logFiles, string backupType, string maxTransferSize)
        {
            var move = CreateMove(backup, dataFiles, logFiles, backupType);

            if (!string.IsNullOrEmpty(move))
            {
                return string.Format("RESTORE DATABASE [{0}] {1} WITH RECOVERY, {2} REPLACE, STATS, MAXTRANSFERSIZE = {3}", databaseName, CreateFrom(backup, false), move, maxTransferSize);
            }
            return null;
        }

        private static string CreateFrom(string backup, bool headersOnly)
        {
            if (IsDirectory(backup))
            {
                var dirInfo = new DirectoryInfo(backup);

                if(!headersOnly)
                {
                    return string.Format(@"FROM DISK = N''{0}\*.*'' ", dirInfo.FullName);
                }
                else
                {
                    var backupFileName = "";

                    foreach (var fi in dirInfo.GetFiles())
                    {
                        if ((fi.Extension == ".sql") || (fi.Extension == ".sqb"))
                        {
                            backupFileName = fi.FullName;
                            break;
                        }
                    }

                    return string.Format(@"FROM DISK = N''{0}'' ", backupFileName);
                }
            }
            else
            {
                return string.Format(@"FROM DISK = N''{0}'' ", backup);
            }
        }

        private static bool ExecuteRedGateRestore(string restoreSql, bool verbose)
        {
            var bOk = false;
            try
            {
                var sqlReturnData = _common.ExecuteSqlReturnData(GetRedGateRestoreSql(restoreSql), true);

                if (sqlReturnData != null)
                {
                    if (sqlReturnData.Tables.Count > 1) //logs
                    {
                        var logs = new string[1];

                        if (verbose)
                        {
                            _common.AddLog("Information", string.Format("Finished: ExecuteRedGateRestore"), true);

                            foreach (DataRow row in sqlReturnData.Tables[0].Rows)
                            {
                                var containsError = row[0].ToString().Contains("error");

                                _common.AddLog(containsError ? "Error" : "Information", row[0].ToString(), true);
                            }
                        }

                        foreach (DataRow row in sqlReturnData.Tables[1].Rows)
                        {
                            var exismsg = "";

                            //get the exitcode
                            if (row["name"].ToString() == "exitcode")
                            {
                                bOk = row["value"].ToString() == "0";

                                exismsg = string.Format("Exit code: {0}", row["value"]);
                            }
                            else if (row["name"].ToString() == "sqlerrorcode")
                            {
                                exismsg = string.Format("SQL error code: {0}", row["value"]);
                            }

                            if (verbose)
                            {
                                _common.AddLog(!bOk ? "Error" : "Success", exismsg, true);
                            }
                        }

                        if (verbose)
                        {
                            _common.AddLog(!bOk ? "Error" : "Information", string.Format("ExecuteRedGateRestore returned: {0}", bOk), true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed executing red gate restore", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return false;
            }

            return bOk;
        }

        private static string GetRedGateRestoreSql(string sql)
        {
            sql = sql.Replace(", STATS", "");
            sql = string.Format("EXECUTE master..sqlbackup '-SQL \"{0}\" '", sql);
            return sql;
        }

        private static string GetRestoreDate(string databaseName)
        {
            var sql = string.Format("USE msdb SELECT TOP 1 restore_date FROM RestoreHistory " +
                                    "WHERE   destination_database_name = '{0}' " +
                                    "ORDER BY restore_date DESC", databaseName);

            var restoreData = _common.ExecuteSqlReturnData(sql, true);

            if (restoreData != null)
            {
                if (restoreData.Tables[0].Rows.Count > 0)
                {
                    return restoreData.Tables[0].Rows[0].ItemArray[0].ToString();
                }
            }

            return null;
        }

        private static bool CreateDirectories(string dataFiles, string logFiles, bool verbose)
        {
            var bOK = false;

            try
            {
                if (!_common.DirectoryExists(dataFiles))
                {
                    if (_common.CreateDirectory(dataFiles))
                    {
                        if (verbose)
                        {
                            _common.AddLog("Information", string.Format("Created directory: {0}", dataFiles), true);
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("Unable to create data directory {0}", dataFiles));
                    }
                }
                if (!_common.DirectoryExists(logFiles))
                {
                    if (_common.CreateDirectory(logFiles))
                    {
                        if (verbose)
                        {
                            _common.AddLog("Information", string.Format("Created directory: {0}", logFiles), true);
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("Unable to create logs directory {0}", logFiles));
                    }
                }
                bOK = true;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed creating directories", false);
                _common.AddLog("Error", string.Format("DataFiles: {0}", dataFiles), false);
                _common.AddLog("Error", string.Format("LogFiles: {0}", logFiles), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }

        private static string CreateMove(string backup, string dataFiles, string logFiles, string backupType)
        {
            string move = null;
            try
            {
                var sql = string.Format("RESTORE FILELISTONLY {0}", CreateFrom(backup, true));

                if (backupType == "rg")
                {
                    sql = GetRedGateRestoreSql(sql);
                }

                var dbFiles = _common.ExecuteSqlReturnData(sql, true);

                if (dbFiles != null)
                {
                    if (dbFiles.Tables[0].Rows.Count > 0)
                    {
                        var dataFilesDir = new DirectoryInfo(dataFiles);
                        var logFilesDir = new DirectoryInfo(logFiles);

                        foreach (DataRow item in dbFiles.Tables[0].Rows)
                        {
                            var logicalName = item["LogicalName"].ToString();
                            var physicalFile = new FileInfo(item["PhysicalName"].ToString());
                            var physicalConfig = _common.GetInstanceConfigValue(logicalName, false);

                            if (!string.IsNullOrEmpty(physicalConfig))
                            {
                                physicalFile = new FileInfo(physicalConfig + "\\" + physicalFile.Name);
                            }
                            else
                            {
                                if ((string) item["Type"] == "D") //data file
                                {
                                    physicalFile = new FileInfo(dataFilesDir.FullName + "\\" + physicalFile.Name);
                                }
                                if ((string) item["Type"] == "L") // log file
                                {
                                    physicalFile = new FileInfo(logFilesDir.FullName + "\\" + physicalFile.Name);
                                }
                                if ((string) item["Type"] == "F") // fulltext catelog
                                {
                                    physicalFile = new FileInfo(dataFilesDir.FullName + "\\" + physicalFile.Name);
                                }
                            }
                            if (physicalFile != null)
                            {
                                move = string.Format("{0} MOVE N''{1}'' TO N''{2}'',", move, logicalName, physicalFile.FullName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed creating moved files", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return null;
            }
            return move;
        }
    }
}