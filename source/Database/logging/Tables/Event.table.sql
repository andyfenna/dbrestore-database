﻿CREATE TABLE [logging].[Event]
(
	[Event] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(150) NOT NULL
)
