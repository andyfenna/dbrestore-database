﻿ALTER TABLE [logging].[EventLog]
    ADD CONSTRAINT [FK_EventLog_DatabaseName] FOREIGN KEY ([DatabaseName]) REFERENCES [infrastructure].[Database] ([DatabaseName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

