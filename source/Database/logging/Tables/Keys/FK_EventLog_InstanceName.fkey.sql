﻿ALTER TABLE [logging].[EventLog]
    ADD CONSTRAINT [FK_EventLog_InstanceName] FOREIGN KEY ([InstanceName]) REFERENCES [infrastructure].[DatabaseInstance] ([InstanceName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

