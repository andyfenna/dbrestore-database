﻿CREATE TABLE [logging].[EventLog_Archive]
(
	[ID] BIGINT IDENTITY(1,1) NOT NULL, 
	[EventLogID] INT NOT NULL, 
	[Event] NVARCHAR(50) NOT NULL,
	[EventType] NVARCHAR(25) NOT NULL,
	[Log] NVARCHAR(MAX) NOT NULL,
	[InstanceName] SYSNAME NOT NULL,
	[DatabaseName] SYSNAME NULL,
	[EventDate] datetime NOT NULL,
	[TimeStamp] datetime NOT NULL DEFAULT GETDATE()
)
GO
