﻿CREATE TABLE [logging].[EventLog]
(
	[ID] INT IDENTITY(1,1) NOT NULL, 	
	[Event] NVARCHAR(50) NOT NULL,
	[EventType] NVARCHAR(25) NOT NULL,
	[Log] NVARCHAR(MAX) NOT NULL,
	[InstanceName] [sys].[sysname] NOT NULL,
	[DatabaseName] [sys].[sysname] NULL,
	[TimeStamp] DATETIME2 NOT NULL DEFAULT SYSDATETIME() 
)
GO