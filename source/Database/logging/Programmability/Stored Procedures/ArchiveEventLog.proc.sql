﻿-- =============================================
-- Author:		Andrew Fenna
-- Create date: 2012-07-07
-- Description:	This procedure archives event logs
-- Modification History:
-- =============================================
CREATE PROCEDURE [logging].[ArchiveEventLog]
	@RetainDays tinyint = 30
AS
BEGIN
		SET XACT_ABORT ON
		SET NOCOUNT ON  
		DECLARE @Event VARCHAR(50) = 'ArchivingEventLog'
  
		BEGIN TRANSACTION	
        BEGIN TRY        
    
			DECLARE @Log XML

			SET @Log = 
			(
				SELECT @Event as "@Event",
					   'Information' as "@Type",
					    @@SERVERNAME AS "@InstanceName",
						NULL AS "@DatabaseName",
					   'Archiving event logs.' as "text()"
				FOR XML PATH('Log'), ROOT('Logs')
			)
			EXEC [logging].[AddEventLog] @Log;


			DELETE [logging].[EventLog]
			OUTPUT	DELETED.[ID], 
					DELETED.[Event], 
					DELETED.[EventType], 
					DELETED.[Log], 
				    DELETED.[InstanceName],
				    DELETED.[DatabaseName],
				    DELETED.[TimeStamp]
			INTO [logging].[EventLog_Archive]
			(
				[EventLogID],
				[Event], 
				[EventType], 
				[Log], 
				[InstanceName],
				[DatabaseName],
				[EventDate]
			)
			WHERE [TimeStamp] < DATEADD(DAY, -@RetainDays, GETDATE());

			IF(@@RowCount) > 0
				BEGIN
					SET @Log = 
					(
						SELECT @Event as "@Event",
								'Success' as "@Type",
								@@SERVERNAME AS "@InstanceName",
								NULL AS "@DatabaseName",
								'Finished archiving event logs.' as "text()"
						FOR XML PATH('Log'), ROOT('Logs')
					)
					EXEC [logging].[AddEventLog] @Log;
                
					COMMIT TRANSACTION              
					RETURN 1			
				END
			ELSE
				BEGIN
					SET @Log = 
					(
						SELECT @Event as "@Event",
								'Error' as "@Type",
								@@SERVERNAME AS "@InstanceName",
								NULL AS "@DatabaseName",
								'Failed archiving event logs.' as "text()"
						FOR XML PATH('Log'), ROOT('Logs')
					)
					EXEC [logging].[AddEventLog] @Log;

					ROLLBACK TRANSACTION              
					RETURN 0
				END    
			
		END TRY
        BEGIN CATCH		     
            DECLARE @errorLog XML = 
			(
				SELECT (	
					SELECT "@Event", "@Type", "text()" FROM (

					SELECT  @Event as "@Event",
							'Error' as "@Type",
							'Failed adding event log.' as "text()"
					UNION ALL
					SELECT @Event as "@Event",
							'Error' as "@Type",
							'Error: '
							+ CONVERT(NVARCHAR(50), ERROR_NUMBER())
							+ ', Severity '
							+ CONVERT(NVARCHAR(5), ERROR_SEVERITY())
							+ ', State '
							+ CONVERT(NVARCHAR(5), ERROR_STATE())
							+ ', Procedure ' 
							+ ISNULL(ERROR_PROCEDURE(),'-') 
							+ ', Line ' 
							+ CONVERT(NVARCHAR(5), ERROR_LINE()) 
							+ ', Message ' 
							+ CONVERT(NVARCHAR(MAX), ERROR_MESSAGE()) as "text()"
					) A
				FOR XML PATH ('Log'), ROOT('Logs')
				) As XmlOutput
			)

			EXEC [logging].[AddEventLog] @errorLog;

			IF XACT_STATE() <> 0 
				BEGIN	
					ROLLBACK TRANSACTION															
				END		
			RETURN 0
        END CATCH 
	END