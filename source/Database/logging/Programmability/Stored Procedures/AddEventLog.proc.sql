﻿-- =============================================
-- Author:		Andrew Fenna
-- Create date: 2012-07-07
-- Description:	This procedure adds event logs
-- Modification History:
-- =============================================
CREATE PROCEDURE [logging].[AddEventLog]
	@Log XML
AS
	BEGIN
  
		SET XACT_ABORT ON
		SET NOCOUNT ON  

		DECLARE @Event VARCHAR(50) = 'AddEventLog'
        DECLARE @ReturnValue AS INT = 0;
     
        BEGIN TRY        

			BEGIN TRANSACTION              

			INSERT [logging].EventLog
				( [Event] ,
					[EventType] ,
					[InstanceName],	
					[DatabaseName],					  
					[Log]
				)
			SELECT tab.col.value('@Event', 'varchar(50)')
					,tab.col.value('@Type', 'varchar(25)')
					,tab.col.value('@InstanceName', 'sysname')
					,tab.col.value('@DatabaseName', 'sysname')
					,tab.col.value('text()[1]', 'varchar(2000)')
			FROM @Log.nodes('/Logs/Log') AS tab(col)
			WHERE tab.col.value('text()[1]', 'varchar(2000)') IS NOT NULL

			IF(@@ROWCOUNT) > 0
				BEGIN
					COMMIT TRANSACTION					
					SET @ReturnValue = 1
				END
			ELSE
				BEGIN
					ROLLBACK TRANSACTION					
				END			

            RETURN @ReturnValue
			
		END TRY
        
        BEGIN CATCH		     

			PRINT 'Error ' + CONVERT(VARCHAR(50), ERROR_NUMBER())
                + ', Severity ' + CONVERT(VARCHAR(5), ERROR_SEVERITY())
                + ', State ' + CONVERT(VARCHAR(5), ERROR_STATE())
                + ', Procedure ' + ISNULL(ERROR_PROCEDURE(), '-') + ', Line '
                + CONVERT(VARCHAR(5), ERROR_LINE()) 
      
            PRINT ERROR_MESSAGE()  

			IF XACT_STATE() <> 0 
				BEGIN	
					ROLLBACK TRANSACTION															
				END		
			RETURN 0
        END CATCH 
	END