﻿ALTER TABLE [restore].[Config]
    ADD CONSTRAINT [FK_Config_Database] FOREIGN KEY ([DatabaseName]) REFERENCES [infrastructure].[Database] ([DatabaseName]) ON DELETE NO ACTION ON UPDATE NO ACTION;