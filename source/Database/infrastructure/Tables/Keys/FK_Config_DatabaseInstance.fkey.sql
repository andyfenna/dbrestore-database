﻿ALTER TABLE [restore].[Config]
    ADD CONSTRAINT [FK_Config_DatabaseInstance] FOREIGN KEY ([InstanceName]) REFERENCES [infrastructure].[DatabaseInstance] ([InstanceName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

