﻿CREATE TABLE [restore].[Config]
(
	[InstanceName]     [sys].[sysname] NOT NULL,
    [DatabaseName]	   [sys].[sysname] NOT NULL,
	[Config] NVARCHAR(50) NOT NULL,
    [Value]	 NVARCHAR(MAX) NOT NULL
)
