﻿IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[restore].[RestoreDatabaseClr]') ) 
    DROP PROCEDURE [restore].[RestoreDatabaseClr]  

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[restore].[RunScriptsClr]') ) 
    DROP PROCEDURE [restore].[RunScriptsClr]  

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[restore].[KillProcessesClr]') ) 
    DROP PROCEDURE [restore].[KillProcessesClr]

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[restore].[RestrictAccessClr]') ) 
    DROP PROCEDURE [restore].[RestrictAccessClr]

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[permissions].[ConfigureDatabaseSchemaUserPermissionsClr]') ) 
    DROP PROCEDURE [permissions].[ConfigureDatabaseSchemaUserPermissionsClr]

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[permissions].[ConfigureDatabaseUserClr]') ) 
    DROP PROCEDURE [permissions].[ConfigureDatabaseUserClr]

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[permissions].[ConfigureDatabaseUserRoleMembershipClr]') ) 
    DROP PROCEDURE [permissions].[ConfigureDatabaseUserRoleMembershipClr]

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[permissions].[ConfigureInstanceLoginClr]') ) 
    DROP PROCEDURE [permissions].[ConfigureInstanceLoginClr]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'logging.ArchiveEventLog') AND type in (N'P', N'PC'))
	EXECUTE logging.ArchiveEventLog 0
GO