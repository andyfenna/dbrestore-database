﻿
:SETVAR svr_name "'.\mssql2012'"
-- This is the server that you are going to restore
:SETVAR database_name "'laterooms'"
-- This is the database name
:SETVAR svn_client_path "N'C:\Program Files (x86)\svn\bin\svn.exe'"
-- This is the svn client that you will use to checkout the
-- repository conatining the sql patch files to apply
-- post database restore
:SETVAR svn_user "'svnuser'"
-- This is the svn user that you will use to connect to the
-- svn repository
:SETVAR svn_password "'svnpassword'"
-- This is the svn password that you will use to connect to the
-- svn repository
:SETVAR svn_url "N'svn://svn_url_to_the_release_scripts'"
-- This is the svn repository that will contain the sql patch files to
-- apply post database restore
:SETVAR svn_local_path "N'C:\temp'"
-- This is the local check out path for sql patch files to apply
-- post database restore
:SETVAR backup_type "'rg'"
-- This is the backup type, you can choose sql or rg (redgate)
:SETVAR path_to_backup_files "N'\\DBRESTORE01\Laterooms\'"
-- This is the UNC path containing the backup files, you can either
-- enter one backup file with the extention, or enter the path if
-- the path contains split files
:SETVAR data_files "N'C:\MSSQL2012\Data'"
-- This is the data files location
:SETVAR log_files "N'C:\MSSQL2012\Data'"
-- This is the logs file path

 MERGE INTO [infrastructure].[Database]AS dest
	USING 
        ( SELECT    $(database_name)
        ) AS src ( DatabaseName )
    ON dest.DatabaseName = src.DatabaseName
    WHEN NOT MATCHED 
        THEN 
	INSERT  (
                DatabaseName                 
			)
                VALUES
            ( 
				$(database_name)
            );	

MERGE INTO [infrastructure].[DatabaseInstance] AS dest
	USING 
        ( SELECT    $(svr_name)
        ) AS src ( InstanceName )
    ON dest.InstanceName = src.InstanceName
    WHEN NOT MATCHED 
        THEN 
	INSERT  (
                InstanceName                 
			)
                VALUES
            ( 
				$(svr_name)
            );	
		
INSERT INTO [restore].[Config] (Config, InstanceName, DatabaseName, Value)
VALUES ('SVNExe',   $(svr_name), $(database_name), $(svn_client_path)),
('SVNUsr',          $(svr_name), $(database_name), $(svn_user)),
('SVNPwd',          $(svr_name), $(database_name), $(svn_password)),
('SVNCheckoutUrl',  $(svr_name), $(database_name), $(svn_url)),
('SVNToDir',        $(svr_name), $(database_name), $(svn_local_path)),
('BackupType',      $(svr_name), $(database_name), $(backup_type)),
('Backup',          $(svr_name), $(database_name), $(path_to_backup_files)),
('DataFiles',       $(svr_name), $(database_name), $(data_files)),
('LogFiles',        $(svr_name), $(database_name), $(log_files)),
('MaxTransferSize', $(svr_name), $(database_name), '1048576');

