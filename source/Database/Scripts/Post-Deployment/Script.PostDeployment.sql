﻿ 
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.RunScriptsClr') ) 
    DROP PROCEDURE dbo.RunScriptsClr  
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.KillProcessesClr') ) 
    DROP PROCEDURE dbo.KillProcessesClr  
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.RestrictsAccessClr') ) 
    DROP PROCEDURE dbo.RestrictsAccessClr 
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.ConfigureDatabaseSchemaUserPermissionsClr') ) 
    DROP PROCEDURE dbo.ConfigureDatabaseSchemaUserPermissionsClr
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.ConfigureDatabaseSchemaUserPermissionsClr') ) 
    DROP PROCEDURE dbo.ConfigureDatabaseSchemaUserPermissionsClr
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.ConfigureDatabaseUserClr') ) 
    DROP PROCEDURE dbo.ConfigureDatabaseUserClr
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.[ConfigureInstanceLoginClr') ) 
    DROP PROCEDURE dbo.ConfigureInstanceLoginClr
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.CheckDBInstanceExistsClr') ) 
    DROP PROCEDURE dbo.CheckDBInstanceExistsClr
GO

CREATE PROCEDURE [restore].RestoreDatabaseClr
@InstanceName NVARCHAR (4000), @DatabaseName NVARCHAR (4000), @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].RestoreDatabaseClr
GO

CREATE PROCEDURE [restore].RunScriptsClr
@InstanceName NVARCHAR (4000), @DatabaseName NVARCHAR (4000), @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].RunScriptsClr
GO

CREATE PROCEDURE [restore].KillProcessesClr
@InstanceName NVARCHAR (4000), @DatabaseName NVARCHAR (4000), @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].KillProcessesClr
GO

CREATE PROCEDURE [restore].RestrictAccessClr
@InstanceName NVARCHAR (4000), @DatabaseName NVARCHAR (4000), @Restrict BIT, @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].RestrictAccessClr
GO

CREATE PROCEDURE [permissions].ConfigureDatabaseSchemaUserPermissionsClr
@InstanceName NVARCHAR (4000), @DatabaseName NVARCHAR (4000), @DatabaseUserName NVARCHAR(4000), @Permission NVARCHAR(4000), @SchemaName NVARCHAR(4000), @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].ConfigureDatabaseSchemaUserPermissionsClr
GO

CREATE PROCEDURE [permissions].ConfigureDatabaseUserClr
@InstanceName NVARCHAR (4000), @DatabaseName NVARCHAR (4000), @DatabaseUserName NVARCHAR(4000), @DefaultSchema NVARCHAR(4000), @LoginName NVARCHAR(4000), @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].ConfigureDatabaseUserClr
GO

CREATE PROCEDURE [permissions].ConfigureDatabaseUserRoleMembershipClr
@InstanceName NVARCHAR (4000), @DatabaseName NVARCHAR (4000), @DatabaseUserName NVARCHAR(4000), @RoleName NVARCHAR(4000), @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].ConfigureDatabaseUserRoleMembershipClr
GO

CREATE PROCEDURE [permissions].ConfigureInstanceLoginClr
@InstanceName NVARCHAR (4000), @LoginName NVARCHAR(4000), @LoginPassword NVARCHAR(4000), @IsWindowsAccount BIT, @Verbose BIT
AS EXTERNAL NAME DBRestoreClr.[DBRestoreClr.StoredProcedures].ConfigureInstanceLoginClr
GO

DECLARE @EventType TABLE
(	
	[Type] NVARCHAR(25) NOT NULL
)

INSERT INTO @EventType ([Type])
VALUES ('Error'), ('Warning'), ('Information') , ('Success');

MERGE INTO logging.EventType AS dest
	USING @EventType AS src
    ON dest.[Type] = src.[Type] 
    WHEN NOT MATCHED 
        THEN 
	INSERT  (
                [Type] 
			)
                VALUES
            ( 
				[Type] 
            );	
GO

DECLARE @Event TABLE 
(
	[Event] NVARCHAR(50) NOT NULL,
    [Description] NVARCHAR(150) NOT NULL
)

INSERT INTO @Event ([Event], [Description])
VALUES ('AddEventLog', 'Event to add an event log to the database.'),
	   ('ArchivingEventLog', 'Event to archive the event logs on the database.'),
	   ('RestrictAccess', 'Event to revoke access and lock the database.'),
	   ('KillProcesses', 'Event to kill processes on the database.'),
	   ('RestoreDatabase', 'Event to restore the database.'),
       ('ConfigureInstanceLogin', 'Event to configure the instance login.'),
	   ('ConfigureDatabaseUser', 'Event to configure the database user.'),
	   ('ConfigureDatabaseUserRoleMembership', 'Event to configure the database user roles.'),
	   ('ConfigureDatabaseSchemaUserPermissions', 'Event to configure the database user permissions.'),
	   ('CheckDBUserExists', 'Event to check if user exists in database.'),
	   ('CheckDBInstanceExists', 'Event to check if database instance exists in restore database and can connect.'),
	   ('GetConfigValue', 'Event to get the instance\database configuration item'),
	   ('RunScripts', 'Event to run scripts.');

MERGE INTO logging.[Event] AS dest
	USING @Event AS src
    ON dest.[Event] = src.[Event] 
    WHEN NOT MATCHED 
        THEN 
	INSERT  (
                [Event],
				[Description]
			)
                VALUES
            ( 
				[Event],
				[Description]
            );	

GO

MERGE INTO infrastructure.DatabaseInstance AS dest
	USING 
        ( SELECT    @@SERVERNAME
        ) AS src ( InstanceName )
    ON dest.InstanceName = src.InstanceName
    WHEN NOT MATCHED 
        THEN 
	INSERT  (
                InstanceName,
				[Description]
			)
                VALUES
            ( 
				@@SERVERNAME,
				'Restore Server'
            );	

GO

MERGE INTO infrastructure.DatabaseInstance AS dest
	USING 
        ( SELECT	'.\mssql2012'
        ) AS src ( InstanceName )
    ON dest.InstanceName = src.InstanceName
    WHEN NOT MATCHED 
        THEN 
	INSERT  (
                InstanceName,
				[Description]
			)
                VALUES
            ( 
				'.\mssql2012',
				'Local Instance'
            );	

GO

EXEC sp_configure 'show advanced options', 1;
RECONFIGURE;
EXEC sp_configure 'Ad Hoc Distributed Queries', 1;
RECONFIGURE;
EXEC sp_configure 'clr enabled' , '1' ;
reconfigure;
GO