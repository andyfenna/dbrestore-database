﻿--This is a sql script of the SQL Server Agent job to schedule the remote restore. Dont forget to setup up a schedule...
:SETVAR svr_name "SRVTEST"
-- This is the server that you are going to restore
:SETVAR database_name "brilliantdatabase"
-- This is the database name

BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 14/08/2013 17:35:02 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'_Weekly_Restore_$(database_name)',
@enabled=1,
@notify_level_eventlog=0,
@notify_level_email=2,
@notify_level_netsend=0,
@notify_level_page=0,
@delete_level=0,
@description=N'No description available.',
@category_name=N'Database Maintenance',
@owner_login_name=N'sa',
@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Restrict Access]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Restrict Access',
@step_id=1,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=6,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'DECLARE @RC AS INT = 0 ,
@InstanceName AS NVARCHAR(50) = ''$(svr_name)'',
@DatabaseName AS NVARCHAR(50)  = ''$(database_name)'',
@Verbose AS BIT = 1,
@Restrict AS BIT = 1;

EXECUTE @RC = [restore].[RestrictAccessClr] @InstanceName, @DatabaseName, @Restrict, @Verbose

IF(@RC ) = 0
BEGIN
RAISERROR(''Restrict Access Failed'', 16, 1);
END
ELSE
BEGIN
SELECT @RC ;
END',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Restore brilliantdatabase Database]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Restore brilliantdatabase Database',
@step_id=2,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'DECLARE @RC AS INT = 0 ,
@InstanceName AS NVARCHAR(50) = ''$(svr_name)'',
@DatabaseName AS NVARCHAR(50)  = ''$(database_name)'',
@Verbose AS BIT = 1;

EXECUTE @RC = [restore].[RestoreDatabaseClr] @InstanceName, @DatabaseName, @Verbose

IF(@RC ) = 0
BEGIN
EXECUTE @RC = [restore].[RestrictAccessClr] @InstanceName, @DatabaseName, 0, @Verbose
raiserror(''Restore Database Failed'', 16, 1);
END
ELSE
BEGIN
SELECT @RC ;
END',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Setup Logins]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Setup Logins',
@step_id=3,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'DECLARE @RC INT;

GO
/* Process the Database logins required................................................. */
/* Required Variables */
DECLARE @RequiredLogins AS TABLE
(
LoginID TINYINT IDENTITY(1, 1) ,
LoginName NVARCHAR(50) ,
LoginPassword NVARCHAR(50) ,
InstanceName SYSNAME ,
WindowsAccount BIT
);
DECLARE @InstanceName AS SYSNAME = ''$(svr_name)'' ,
@CurrentLogin AS NVARCHAR(50) ,
@CurrentLoginID AS INT ,
@LoginsCount AS INT ,
@CurrentLoginPassword AS NVARCHAR(50) ,
@IsCurrentLoginWindowsAccount BIT ,
@RC INT;

/* SETUP START ------------------------------------------------------------------------- */
INSERT  INTO @RequiredLogins
( LoginName ,
LoginPassword ,
InstanceName ,
WindowsAccount
)
SELECT  LoginName ,
LoginPassword ,
InstanceName ,
IsWindowsAccount
FROM    [permissions].[Login]
WHERE   InstanceName = @InstanceName;
SET @CurrentLoginID = 1;
SELECT  @LoginsCount = COUNT(*)
FROM    @RequiredLogins AS rl;
/* SETUP END --------------------------------------------------------------------------- */

WHILE @CurrentLoginID <= @LoginsCount
BEGIN
SELECT  @CurrentLogin = LoginName ,
@CurrentLoginPassword = LoginPassword ,
@IsCurrentLoginWindowsAccount = WindowsAccount
FROM    @RequiredLogins AS rl
WHERE   LoginID = @CurrentLoginID;

EXECUTE @RC = [permissions].[ConfigureInstanceLoginClr]
@InstanceName = @InstanceName,
@LoginName = @CurrentLogin,
@LoginPassword = @CurrentLoginPassword,
@IsWindowsAccount = @IsCurrentLoginWindowsAccount,
@Verbose = 1;
PRINT @RC
SET @CurrentLoginID = @CurrentLoginID + 1;
END

GO
',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Setup Users]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Setup Users',
@step_id=4,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'/* Process the Database users required................................................. */
/* Required Variables */
DECLARE @RequiredUsers AS TABLE
(
RequiredUserID TINYINT IDENTITY(1, 1) ,
UserName NVARCHAR(50) ,
UserDefaultSchema NVARCHAR(50) ,
DatabaseName SYSNAME
);

DECLARE @InstanceName AS SYSNAME = ''$(svr_name)'',
@CurrentRequiredUserID AS INT,
@CurrentUserName AS NVARCHAR(50),
@CurrentUserDefaultSchema AS NVARCHAR(50),
@CurrentUserDatabase AS NVARCHAR(50),
@UsersCount AS INT,
@RC INT;

/* SETUP START ------------------------------------------------------------------------- */
INSERT  INTO @RequiredUsers
( UserName ,
UserDefaultSchema ,
DatabaseName
)
SELECT  UserName ,
DefaultSchema ,
DatabaseName
FROM    [permissions].[User] AS u
WHERE   InstanceName = @InstanceName;
SET @CurrentRequiredUserID = 1;
SELECT  @UsersCount = COUNT(*)
FROM    @RequiredUsers AS ru;
/* SETUP END --------------------------------------------------------------------------- */

WHILE @CurrentRequiredUserID <= @UsersCount
BEGIN
SELECT  @CurrentUserName = UserName ,
@CurrentUserDefaultSchema = UserDefaultSchema ,
@CurrentUserDatabase = DatabaseName
FROM    @RequiredUsers AS ru
WHERE   RequiredUserID = @CurrentRequiredUserID;

EXECUTE @RC = [permissions].[ConfigureDatabaseUserClr]
@InstanceName = @InstanceName,
@DatabaseUserName = @CurrentUserName,
@DatabaseName = @CurrentUserDatabase,
@DefaultSchema = @CurrentUserDefaultSchema,
@LoginName = @CurrentUserName,
@Verbose = 1;

SET @CurrentRequiredUserID = @CurrentRequiredUserID + 1;
END

GO
',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Setup User Roles]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Setup User Roles',
@step_id=5,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'/*Process the Database users role requirements required..................*/
/* Required Variables */

DECLARE @RequiredUserRoleMemberships AS TABLE
(
UserRoleID INT IDENTITY(1, 1) ,
UserName NVARCHAR(50) ,
RoleName NVARCHAR(50) ,
DatabaseName SYSNAME
);

DECLARE @InstanceName AS SYSNAME = ''$(svr_name)'' ,
@CurrentUserRoleID AS INT ,
@CurrentUserName AS NVARCHAR(50) ,
@CurrentUserRole AS NVARCHAR(50) ,
@CurrentUserRoleDatabaseName AS NVARCHAR(50) ,
@UserRoleCount AS INT ,
@RC INT;

/* SETUP START ------------------------------------------------------------------------- */
SET @CurrentUserRoleID = 1
INSERT  INTO @RequiredUserRoleMemberships
( UserName ,
RoleName ,
DatabaseName
)
SELECT  UserName ,
RoleName ,
DatabaseName
FROM    [permissions].UserRoleMembership AS urm
WHERE   InstanceName = @InstanceName
SET @CurrentUserRoleID = 1
SELECT  @UserRoleCount = COUNT(*)
FROM    @RequiredUserRoleMemberships AS ru;
/* SETUP END --------------------------------------------------------------------------- */

WHILE @CurrentUserRoleID <= @UserRoleCount
BEGIN
SELECT  @CurrentUserName = UserName ,
@CurrentUserRole = RoleName ,
@CurrentUserRoleDatabaseName = DatabaseName
FROM    @RequiredUserRoleMemberships AS rurm
WHERE   UserRoleID = @CurrentUserRoleID;

EXECUTE @RC = [permissions].[ConfigureDatabaseUserRoleMembershipClr] @InstanceName = @InstanceName,
@DatabaseUserName = @CurrentUserName,
@DatabaseName = @CurrentUserRoleDatabaseName,
@RoleName = @CurrentUserRole, @Verbose = 1;
SET @CurrentUserRoleID = @CurrentUserRoleID + 1;
END
GO',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Setup User Permissions]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Setup User Permissions',
@step_id=6,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'/*Process the Database users permissions required..................*/
/* Required Variables */

DECLARE @RequiredUserPermissions AS TABLE
(
UserPermissionID INT IDENTITY(1, 1) ,
UserName NVARCHAR(50) ,
SchemaName NVARCHAR(50) ,
PermissionType NVARCHAR(50) ,
DatabaseName SYSNAME
);

DECLARE @InstanceName AS SYSNAME = ''$(svr_name)'',
@CurrentUserPermissionID AS INT,
@CurrentUserName AS NVARCHAR(50),
@CurrentSchemaName AS NVARCHAR(50),
@CurrentPermissionType AS NVARCHAR(50),
@CurrentUserPermissionDatabaseName AS NVARCHAR(50),
@UserPermissionCount AS INT,
@RC INT;

/* SETUP START ------------------------------------------------------------------------- */
SET @CurrentUserPermissionID = 1
INSERT  INTO @RequiredUserPermissions
( UserName ,
SchemaName ,
PermissionType ,
DatabaseName
)
SELECT  UserName ,
SchemaName ,
PermissionType ,
DatabaseName
FROM    [permissions].UserPermission
WHERE   InstanceName = @InstanceName
SET @CurrentUserPermissionID = 1

SELECT  @UserPermissionCount = COUNT(*)
FROM    @RequiredUserPermissions AS rup;

SELECT  *
FROM    @RequiredUserPermissions AS RUP

/* SETUP END --------------------------------------------------------------------------- */

WHILE @CurrentUserPermissionID <= @UserPermissionCount
BEGIN
SELECT  @CurrentUserName = UserName ,
@CurrentSchemaName = SchemaName ,
@CurrentPermissionType = PermissionType ,
@CurrentUserPermissionDatabaseName = DatabaseName
FROM    @RequiredUserPermissions AS rup
WHERE   UserPermissionID = @CurrentUserPermissionID;

EXEC @RC = [permissions].[ConfigureDatabaseSchemaUserPermissionsClr]
@InstanceName = @InstanceName,
@DatabaseName = @CurrentUserPermissionDatabaseName,
@DatabaseUserName = @CurrentUserName,
@Permission = @CurrentPermissionType,
@SchemaName = @CurrentSchemaName,
@Verbose = 1;

SET @CurrentUserPermissionID = @CurrentUserPermissionID + 1;

END
GO

',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Grant Access]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Grant Access',
@step_id=7,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'DECLARE @RC AS INT = 0 ,
@InstanceName AS NVARCHAR(50) = ''$(svr_name)'',
@DatabaseName AS NVARCHAR(50)  = ''$(database_name)'',
@Verbose AS BIT = 1,
@Restrict AS BIT = 0;

EXECUTE @RC = [restore].[RestrictAccessClr] @InstanceName, @DatabaseName, @Restrict, @Verbose

IF(@RC ) = 0
BEGIN
raiserror(''Unrestrict Access Failed'', 16, 1);
END
ELSE
BEGIN
SELECT @RC
END',
@database_name=N'DBRestore',
@flags=4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Run release scripts]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Run release scripts',
@step_id=8,
@cmdexec_success_code=0,
@on_success_action=3,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'DECLARE @RC AS INT ,
@InstanceName AS NVARCHAR(50) = ''$(svr_name)'',
@DatabaseName AS NVARCHAR(50)=  ''$(database_name)'',
@Verbose AS BIT = 1;

EXEC @RC = [restore].[RunScriptsClr]
@InstanceName = @InstanceName,
@DatabaseName = @DatabaseName,
@Verbose = 1;

IF(@RC)= 0
BEGIN
raiserror(''Run release scripts'', 16, 1);
END
ELSE
BEGIN
SELECT @RC;
END',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Archive event logs after 30 days]    Script Date: 14/08/2013 17:35:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Archive event logs after 30 days',
@step_id=9,
@cmdexec_success_code=0,
@on_success_action=1,
@on_success_step_id=0,
@on_fail_action=2,
@on_fail_step_id=0,
@retry_attempts=0,
@retry_interval=0,
@os_run_priority=0, @subsystem=N'TSQL',
@command=N'USE [DBRestore]
GO

DECLARE    @return_value int

EXEC    @return_value = [logging].[ArchiveEventLog]
@RetainDays = 30

SELECT    ''Return Value'' = @return_value

GO',
@database_name=N'DBRestore',
@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Sundays @ 0900',
@enabled=1,
@freq_type=8,
@freq_interval=1,
@freq_subday_type=1,
@freq_subday_interval=0,
@freq_relative_interval=0,
@freq_recurrence_factor=1,
@active_start_date=20121005,
@active_end_date=99991231,
@active_start_time=90000,
@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO