using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;
using System.Threading;

namespace DBRestoreClr
{
    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Configures a database user role membership.
        /// </summary>
        /// <param name="InstanceName">Instance to configure on.</param>
        /// <param name="DatabaseName">Database to configure permissions on.</param>
        /// <param name="DatabaseUserName">Database user name to configure permissionson.</param>
        /// <param name="RoleName">Role to configure .</param>
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "ConfigureDatabaseUserRoleMembershipClr")]
        public static int ConfigureDatabaseUserRoleMembershipClr(string InstanceName, string DatabaseName, string DatabaseUserName, string RoleName, bool Verbose)
        {
            try
            {
                _common = new Common("ConfigureDatabaseUserRoleMembership", InstanceName, DatabaseName, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information",  string.Format("Started configuring database user: {0} for role membership {1} ", DatabaseUserName, RoleName), true);

                    if (_common.CheckForDBUserRoleMembership(DatabaseUserName, RoleName))
                    {
                        _common.AddLog("Success", string.Format("The user: {0} already already a member of role: {1}", DatabaseUserName, RoleName), true);
                        return 1;
                    }
                    else
                    {
                        if (SetDBUserRoleMembership(DatabaseUserName, RoleName))
                        {
                            _common.AddLog("Success", string.Format("The user: {0} is now a member of the database role: {1}", DatabaseUserName, RoleName), true);
                            return 1;
                        }
                        _common.AddLog("Error", string.Format("Failed to set the user: {0} to database role {1}", DatabaseUserName, RoleName), true);
                        return 0;
                    }
                }
                _common.AddLog("Error", string.Format("Failed configuring database user: {0} for role membership {1} ", DatabaseUserName, RoleName), true);
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed configuring database user role membership", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool SetDBUserRoleMembership(string databaseUserName, string roleName)
        {
            var bOK = false;

            try
            {
                var sql = string.Format("EXEC sp_addrolemember N'{0}', N'{1}'", roleName, databaseUserName);

                bOK = _common.ExecuteSql(sql, true, false);

                if(_common.CheckForDBUserRoleMembership(databaseUserName, roleName))
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed setting database user role", false);
                _common.AddLog("Error", string.Format("User: {0}", databaseUserName), false);
                _common.AddLog("Error", string.Format("Role: {0}", roleName), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }
    }
}