using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;
using System.Threading;

namespace DBRestoreClr
{
    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Configures a database user.
        /// </summary>
        /// <param name="InstanceName">Instance to configure on.</param>
        /// <param name="DatabaseName">Database to configure permissions on.</param>
        /// <param name="DatabaseUserName">Database user name to configure permissionson.</param>
        /// <param name="DefaultSchema">DefaultSchema to configure .</param>
        /// <param name="LoginName">LoginName to configure.</param>        
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "ConfigureDatabaseUserClr")]
        public static int ConfigureDatabaseUserClr(string InstanceName, string DatabaseName, string DatabaseUserName, string DefaultSchema, string LoginName, bool Verbose)
        {
            try
            {
                _common = new Common("ConfigureDatabaseUser", InstanceName, DatabaseName, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information", "Started configuring database user", true);

                    if (_common.CheckForUser(DatabaseUserName))
                    {
                        _common.AddLog("Warning", string.Format("The db user: {0} already exists", DatabaseUserName), true);

                        if (AssociateUser(DatabaseUserName, LoginName))
                        {
                            _common.AddLog("Success", string.Format("User: {0} already existed and has been re-associated to Login: {1}", DatabaseUserName, LoginName), true);
                            return 1;
                        }
                    }
                    else
                    {
                        if (CreateUser(DatabaseUserName, LoginName, DefaultSchema))
                        {
                            _common.AddLog("Success", string.Format("User: {0} has been created", DatabaseUserName), true);
                            return 1;
                        }
                    }
                }
                _common.AddLog("Error", string.Format("Failed configuring database user: {0} login: {1} default schema {2}", DatabaseUserName, LoginName, DefaultSchema), true);
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed configuring database user", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool CreateUser(string databaseUserName, string loginName, string defaultSchema)
        {
            var bOK = false;

            try
            {
                var sql = string.Format("CREATE USER [{0}] FOR LOGIN [{1}] WITH DEFAULT_SCHEMA = {2}", databaseUserName, loginName, defaultSchema);

                if(_common.ExecuteSql(sql, true, false))
                {
                    if (_common.CheckForUser(databaseUserName))
                    {
                        bOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed creating database user", false);
                _common.AddLog("Error", string.Format("User: {0}", databaseUserName), false);
                _common.AddLog("Error", string.Format("Login: {0}", loginName), false);
                _common.AddLog("Error", string.Format("Default Schema: {0}", defaultSchema), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }

        private static bool AssociateUser(string databaseUserName, string loginName)
        {
            var bOK = false;

            try
            {
                var sql = string.Format("EXEC sp_change_users_login 'Update_One' , '{0}', '{1}'", databaseUserName, loginName);

                if (_common.ExecuteSql(sql, true, false))
                {
                    if (_common.CheckForUser(databaseUserName))
                    {
                        bOK = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed altering database user", false);
                _common.AddLog("Error", string.Format("User: {0}", databaseUserName), false);
                _common.AddLog("Error", string.Format("Login: {0}", loginName), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }  
    }
}