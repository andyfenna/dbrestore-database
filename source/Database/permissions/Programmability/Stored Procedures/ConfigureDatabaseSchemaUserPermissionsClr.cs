using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;
using System.Threading;

namespace DBRestoreClr
{
    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Grants permission on schema for a database instance.
        /// </summary>
        /// <param name="InstanceName">Instance to configure on.</param>
        /// <param name="DatabaseName">Database to configure permissions on.</param>
        /// <param name="DatabaseUserName">Database user name to configure permissionson.</param>
        /// <param name="Permission">Permission to configure.</param>
        /// <param name="SchemaName">SchemaName to configure permissions on.</param>
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "ConfigureDatabaseSchemaUserPermissionsClr")]
        public static int ConfigureDatabaseSchemaUserPermissionsClr(string InstanceName, string DatabaseName, string DatabaseUserName, string Permission, string SchemaName, bool Verbose)
        {
            try
            {
                _common = new Common("ConfigureDatabaseSchemaUserPermissions", InstanceName, DatabaseName, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information", "Started configuring database schema user permissions", true);

                    if (_common.CheckForDBSchemaUserPermissions(DatabaseUserName, Permission, SchemaName))
                    {
                        _common.AddLog("Success", string.Format("The user: {0} already has permission {1} on SCHEMA::{2}", DatabaseUserName, Permission, SchemaName), true);
                        return 1;
                    }
                    else
                    {
                        if (SetDBSchemaUserPermissions(DatabaseUserName, Permission, SchemaName))
                        {
                            _common.AddLog("Success", string.Format("The user: {0} now has permission {1} on SCHEMA::{2}", DatabaseUserName, Permission, SchemaName), true);
                            return 1;
                        }
                        _common.AddLog("Error", string.Format("Failed to set the user: {0} permission {1} on SCHEMA::{2}", DatabaseUserName, Permission, SchemaName), true);
                        return 0;
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed configuring database schema user permissions", false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool SetDBSchemaUserPermissions(string databaseUserName, string permission, string schemaName)
        {
            var bOK = false;

            try
            {
                 var sql = string.Format("GRANT {0} ON SCHEMA::{1} TO [{2}]", permission, schemaName, databaseUserName);

                if(_common.ExecuteSql(sql, true, false))
                {
                    if (_common.CheckForDBSchemaUserPermissions(databaseUserName, permission, schemaName))
                    {
                        bOK = true;
                    }
                }                    
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed configuring database schema user permissions", false);
                _common.AddLog("Error", string.Format("User: {0}", databaseUserName), false);
                _common.AddLog("Error", string.Format("Permission: {0}", permission), false);
                _common.AddLog("Error", string.Format("Schema: {0}", schemaName), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }        
    }
}