using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.IO;
using System.Threading;

namespace DBRestoreClr
{
    public partial class StoredProcedures
    {
        /// <summary>
        /// Author:		    Andrew Fenna.
        /// Create date:    2013-03-06.
        /// Description:    Configures a database instance login.
        /// </summary>
        /// <param name="InstanceName">Instance to configure.</param>
        /// <param name="LoginName">Login name to configure.</param>
        /// <param name="LoginPassword">Login password to configure.</param>
        /// <param name="IsWindowsAccount">Is a windows acct or sql.</param>        
        /// <param name="Verbose">Sets whether to use verbose logging.</param>
        /// <returns>1= success, 0 = failure.</returns>
        [SqlProcedure(Name = "ConfigureInstanceLoginClr")]
        public static int ConfigureInstanceLoginClr(string InstanceName, string LoginName, string LoginPassword, bool IsWindowsAccount, bool Verbose)
        {
            try
            {
                _common = new Common("ConfigureInstanceLogin", InstanceName, null, Verbose);

                if (_common.CheckInstance(InstanceName, true))
                {
                    _common.AddLog("Information", string.Format("Started configuring login: {0}", LoginName), true);

                    if (_common.CheckForLogin(LoginName))
                    {
                        _common.AddLog("Success", string.Format("The login: {0} already exists", LoginName), true);
                        return 1;
                    }
                    else
                    {
                        if (CreateLogin(LoginName, LoginPassword, IsWindowsAccount))
                        {
                            _common.AddLog("Success", string.Format("The login: {0} now exists", LoginName), true);
                            return 1;
                        }
                        _common.AddLog("Error", string.Format("Failed to add the login: {0}", LoginName), true);
                        return 0;
                    }
                }
                _common.AddLog("Error", string.Format("Failed configuring login: {0}", LoginName), true);
                return 0;
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", string.Format("Failed configuring login: {0}", LoginName), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                return 0;
            }
        }

        private static bool CreateLogin(string loginName, string loginPassword, bool isWindowsAccount)
        {
            var bOK = false;

            try
            {
                var sql = "";
                
                if(isWindowsAccount)
                {
                    sql = string.Format("CREATE LOGIN [{0}] FROM WINDOWS", loginName);
                }
                else
                {
                    sql = string.Format("CREATE LOGIN [{0}] WITH PASSWORD = '{1}', CHECK_POLICY = OFF,  CHECK_EXPIRATION = OFF", loginName, loginPassword);
                }

                bOK = _common.ExecuteSql(sql, true, false);

                if (_common.CheckForLogin(loginName))
                {
                    bOK = true;
                }
            }
            catch (Exception ex)
            {
                _common.AddLog("Error", "Failed setting database user role", false);
                _common.AddLog("Error", string.Format("Login: {0}", loginName), false);
                _common.AddLog("Error", string.Format("Error: {0}", ex.Message), true);
                bOK = false;
            }

            return bOK;
        }
    }
}