﻿CREATE TABLE [permissions].[UserRoleMembership] (
    [UserName]     NVARCHAR (50) NOT NULL,
    [RoleName]     NVARCHAR (50) NOT NULL,
    [DatabaseName] [sys].[sysname] NOT NULL,
    [InstanceName] [sys].[sysname] NOT NULL
);

