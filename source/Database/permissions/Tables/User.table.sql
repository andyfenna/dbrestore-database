﻿CREATE TABLE [permissions].[User] (
    [UserName]      NVARCHAR (50) NOT NULL,
    [DefaultSchema] NVARCHAR (50) NOT NULL,
    [DatabaseName]  [sys].[sysname] NOT NULL,
    [InstanceName]  [sys].[sysname] NOT NULL
);

