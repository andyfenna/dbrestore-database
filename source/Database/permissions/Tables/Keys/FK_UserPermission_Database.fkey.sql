﻿ALTER TABLE [permissions].[UserPermission]
    ADD CONSTRAINT [FK_UserPermission_Database] FOREIGN KEY ([DatabaseName]) REFERENCES [infrastructure].[Database] ([DatabaseName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

