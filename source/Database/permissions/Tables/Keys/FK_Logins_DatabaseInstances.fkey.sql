﻿ALTER TABLE [permissions].[Login]
    ADD CONSTRAINT [FK_Logins_DatabaseInstance] FOREIGN KEY ([InstanceName]) REFERENCES [infrastructure].[DatabaseInstance] ([InstanceName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

