﻿ALTER TABLE [permissions].[User]
    ADD CONSTRAINT [FK_Users_DatabaseInstance] FOREIGN KEY ([InstanceName]) REFERENCES [infrastructure].[DatabaseInstance] ([InstanceName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

