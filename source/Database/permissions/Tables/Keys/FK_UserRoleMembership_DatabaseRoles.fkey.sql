﻿ALTER TABLE [permissions].[UserRoleMembership]
    ADD CONSTRAINT [FK_UserRoleMembership_DatabaseRoles] FOREIGN KEY ([RoleName]) REFERENCES [permissions].[DatabaseRole] ([RoleName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

