﻿ALTER TABLE [permissions].[UserPermission]
    ADD CONSTRAINT [FK_UserPermission_ObjectPermissionType] FOREIGN KEY ([PermissionType]) REFERENCES [permissions].[PermissionType] ([PermissionType]) ON DELETE NO ACTION ON UPDATE NO ACTION;

