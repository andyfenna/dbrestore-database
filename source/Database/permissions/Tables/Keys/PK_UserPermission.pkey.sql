﻿ALTER TABLE [permissions].[UserPermission]
    ADD CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED ([UserName] ASC, [SchemaName] ASC, [PermissionType] ASC, [DatabaseName] ASC, [InstanceName] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);

