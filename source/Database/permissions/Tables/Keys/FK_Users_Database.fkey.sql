﻿ALTER TABLE [permissions].[User]
    ADD CONSTRAINT [FK_Users_Database] FOREIGN KEY ([DatabaseName]) REFERENCES [infrastructure].[Database] ([DatabaseName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

