﻿ALTER TABLE [permissions].[UserRoleMembership]
    ADD CONSTRAINT [FK_UserRoleMembership_DatabaseInstance] FOREIGN KEY ([InstanceName]) REFERENCES [infrastructure].[DatabaseInstance] ([InstanceName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

