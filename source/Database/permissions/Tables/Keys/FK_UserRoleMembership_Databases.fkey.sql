﻿ALTER TABLE [permissions].[UserRoleMembership]
    ADD CONSTRAINT [FK_UserRoleMembership_Database] FOREIGN KEY ([DatabaseName]) REFERENCES [infrastructure].[Database] ([DatabaseName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

