﻿ALTER TABLE [permissions].[UserPermission]
    ADD CONSTRAINT [FK_UserPermission_DatabaseInstance] FOREIGN KEY ([InstanceName]) REFERENCES [infrastructure].[DatabaseInstance] ([InstanceName]) ON DELETE NO ACTION ON UPDATE NO ACTION;

