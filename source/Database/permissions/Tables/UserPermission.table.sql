﻿CREATE TABLE [permissions].[UserPermission] (
    [UserName]       NVARCHAR (50) NOT NULL,
    [SchemaName]     NVARCHAR (50) NOT NULL,
    [PermissionType] NVARCHAR (50) NOT NULL,
    [DatabaseName]   [sys].[sysname] NOT NULL,
    [InstanceName]   [sys].[sysname] NOT NULL
);

